!========1=========2=========3=========4=========5=========6=========7=========8
!
! ILC RTML e+ second stage bunch compressor, PBC2.XSIF
!
! Lattice file for BC2, which accelerates from 5 GeV to 15 GeV and also
! compresses the beam.
!
! This lattice requires the following files to run properly:
!
!    RTML_common.xsif
!
! plus one of
!
!    RTML_sigz_nominal.xsif
!    RTML_sizg300A.xsif
!    RTML_sigzLowN.xsif
!    RTML_sigz150A.xsif
!
! plus one of
!
!    RTML_BC2_noextract.xsif
!    RTML_BC2_tuneup.xsif
!    RTML_BC2_abort.xsif
!
! and, of course,
!
!    PBC2_DL.xsif   
!                   
! ==============================================================================
! 15-Apr-2015, M. Woodley (SLAC)
!    ILC2015a release
! 10-Feb-2012, AV:
!          Add a RF Unit to BC2RF.
!
! 26-apr-2007, PT:
!    reduce matching bend length from 1.8 m to 1.6 m, per V. Kashikhin.
! 10-Apr-2007, PT:
!    add HGAPs to sector bends.
! 09-Apr-2007, PT:
!    add a second SPBC2 and ABBC2 set in the wiggler; adjust collimator
!    gaps for increased energy spread and changed dispersion functions.
! 09-MAR-2007, M. Woodley:
!    fix lengths of BC quads and drifts; adjust matching into and out of BC2
!    rf for better chromatic behavior
! 12-JAN-2007, PT:
!    eliminate PPS stoppers
! 11-DEC-2006, PT:
!    eliminate x correctors at D quads.  Move BPMs to corrector side of each
!    quad; eliminate 3 LOLA cavities; reduce # of RF units to 16 (15 active +
!    1 spare), with quad spacing of 1 quad / 3 CMs
! 01-MAR-2006, PT:
!    file created
! 24-AUG-2012, S. Seletskiy:         
!    lengths of some drifts and lengths of focusing, defocusing and the         
!    tuning quads inside the wiggler were corrected          
! ------------------------------------------------------------------------------       
!             
  
! moved CALL to pRTML.mad8
! CALL, FILENAME="../../../ILC2012e_AV/deckfiles/RTML/PBC2_DL.xsif"      
  
! ==============================================================================
! CMs
! ------------------------------------------------------------------------------

! The system uses a total of 57 cryomodules, of which 54 are needed and 3 are
! spare.  There are quads in every 2nd cryomodule (different from the main
! linac).

  BC2CAV : ILCCAV, DELTAE=BC2DELTAE, PHI0=BC2PHI0, ELOSS=BC2ELOSS, &
    LFILE="../../auxfiles/wakes/ilc2005.lwake.sr.data", &         
    TFILE="../../auxfiles/wakes/ilc2005.twake.sr.data"            

  BC2SPARE : BC2CAV, DELTAE = 0.

! For matching purposes, here is a marker at the end of the 
! matching quad section (beginning of klys 4 region)

  BC2MATCH : MARKER
  bc2match2 : marker

! Here are the nominal Twiss parameters at the fit point

  BC2MATCH_TWISS : BETA0, BETX = 40.7487, ALFX = -1.0233, &
                          BETY = 59.7841, ALFY =  1.4599

! Anyway, here is the lattice, organized by RF sources (3 CMs):

  BC2RF : LINE = ( DSEGBOX, &
!
! klystron 1
!
    CM(BC2CAV), DMM, CMDQUAD(QMATCH1CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 2
!
    CM(BC2CAV), DMM, CMFQUAD(QMATCH2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 3
!
    CM(BC2CAV), DMM, CMDQUAD(QMATCH3CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 4
!
    CM(BC2CAV), DMM, CMFQUAD(QMATCH4CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM, DENDBOX,                        &
!
! klystron 5
!
    CM(BC2CAV), DMM, CMDQUAD(QMATCH5CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 6
!
    CM(BC2CAV), DMM, CMFQUAD(QFBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 7
!
    CM(BC2CAV), DMM, CMDQUAD(QDBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 8
!
    CM(BC2CAV), DMM, CMFQUAD(QFBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM, DENDBOX,                      &
!
! klystron 9
!
    BC2MATCH, &         
    CM(BC2CAV), DMM, CMDQUAD(QDBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 10
!
    CM(BC2CAV), DMM, CMFQUAD(QFBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 11
!
    CM(BC2CAV), DMM, CMDQUAD(QDBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                               &
!
! klystron 12
!
    CM(BC2CAV), DMM, CMFQUAD(QFBC2CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM, DENDBOX,                      &
!
! klystron 13
!
    BC2MATCH, &         
    CM(BC2CAV), DMM, CMFQUAD(QMATCH6CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 14
!
    CM(BC2CAV), DMM, CMDQUAD(QMATCH7CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 15
!
    CM(BC2CAV), DMM, CMFQUAD(QMATCH8CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM,                                 &
!
! klystron 16 is the spare in this case  (NOT ANYMORE)
!
    CM(BC2CAV), DMM, CMDQUAD(QMATCH9CM,BC2CAV), DMM, &
    CM(BC2CAV), DMM, DSEGBOX                              )

! ==============================================================================
! Bends
! ------------------------------------------------------------------------------

! rectangular length of BC2 bend, and initial angle into wiggler

  LBC2BEND  := 0.9
  LBC2LONGMBEND := 1.8
  LBC2MBEND := 1.6
  LBC2M := (LBC2LONGMBEND-LBC2MBEND) * BC2ETAP &
         /(2*SIN(BC2ETAP/2))
!  BC2ETAP := 0.028

! bend magnets which establish the initial angle -- basically a 4-bend
! chicane which is missing its last bend

  BC2MB1 : SBEND, TYPE="D25L1600", ANGLE = -BC2ETAP, E1 = -BC2ETAP/2, & 
                  E2 = -BC2ETAP/2, HGAP = GB30 / 2,                   & 
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  
  BC2MB2 : SBEND, TYPE="D25L1600", ANGLE =  BC2ETAP, E1 = BC2ETAP/2,  & 
                  E2 = BC2ETAP/2, HGAP = GB30 / 2,                    & 
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  
  BC2MB3 : SBEND, TYPE="D25L1600", ANGLE =  BC2ETAP, E1 = BC2ETAP/2,  & 
                  E2 =  BC2ETAP/2, HGAP = GB30 / 2,                   &
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  

! Dispersion suppressor is a mirror image of the matcher

  BC2MB4 : SBEND, TYPE="D25L1600", ANGLE = -BC2ETAP, E1 = -BC2ETAP/2, & 
                  E2 = -BC2ETAP/2, HGAP = GB30 / 2,                   & 
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  
  BC2MB5 : SBEND, TYPE="D25L1600", ANGLE = -BC2ETAP, E1 = -BC2ETAP/2, & 
                  E2 = -BC2ETAP/2, HGAP = GB30 / 2,                   & 
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  
  BC2MB6 : SBEND, TYPE="D25L1600", ANGLE =  BC2ETAP, E1 = BC2ETAP/2,  & 
                  E2 = BC2ETAP/2, HGAP = GB30 / 2,                    & 
                  L = LBC2MBEND * BC2ETAP/(2*SIN(BC2ETAP/2))  

! 4-bend chicane which moves the beam 2 cm in x, to separate the low-energy
! electrons and high-energy gammas generated at the laser wire from the rest
! of the beam

  BBC2CH1 : SBEND, TYPE="D25L1600", L = 1.6, ANGLE = -0.0125,     & 
                   E1 = -0.00625, E2 = -0.00625, HGAP = GB30 / 2
  BBC2CH2 : SBEND, TYPE="D25L1600", L = 1.6, ANGLE =  0.0125,     & 
                   E1 =  0.00625, E2 =  0.00625, HGAP = GB30 / 2 

! Pulsed bend for tuneup extraction.  Here we represent it as an HKICK, since
! it deflects the beam with respect to the survey axis of the downstream
! elements

  BEXT_BC2: HKICK, TYPE="PULSED BEND", L=0.5, KICK=-BC2_EXT_ANGLE/2

! Septum bends:  each is 1 meter long with a max 0.1 T pole-tip field on the
! bending side, leading to a deflection angle of 2.0 mrad per bend at 15 GeV
! Also, to get them to bend the same direction as the kickers and the pulsed
! bend (represented here as a kicker), we need the opposite sign of deflection.
!
!  BC2SEPT_A : SBEND, TYPE="SEPTUM BEND", L = 0.5, ANGLE = 1.0E-03, & 
!                     E1 = 1.0E-03, E2 = 0, FINT = 0.5, FINTX = 0
!  BC2SEPT_B : SBEND, TYPE="SEPTUM BEND", L = 0.5, ANGLE = 1.0E-03, & 
!                     E1 = 0, E2 = 1.0E-03, FINT = 0, FINTX = 0.5
!  BC2SEPT : LINE = (BC2SEPT_A, BC2SEPT_B)
!
!! Regular extraction line bends, 0.8 T pole fields and 2 m long
!
!  BBC2DL1_A : SBEND, TYPE="EXTRACTION BEND", L = 1.0, ANGLE = 16E-3, & 
!                     E1 = 16E-3, E2 = 0, FINT = 0.5, FINTX = 0
!  BBC2DL1_B : SBEND, TYPE="EXTRACTION BEND", L = 1.0, ANGLE = 16E-3, & 
!                     E1 = 0, E2 = 16E-3, FINT = 0, FINTX = 0.5
!
!  BBC2DL1 : LINE = (BBC2DL1_A, BBC2DL1_B) 
!
!! 1.6 T pole tip field and 2 meters long
!
!  BBC2DL2_A : SBEND, TYPE="EXTRACTION BEND", L = 1.0, ANGLE = 32E-3, & 
!                     E1 = 32E-3, E2 = 0, FINT = 0.5, FINTX = 0
!  BBC2DL2_B : SBEND, L = 1.0, TYPE="EXTRACTION BEND", ANGLE = 32E-3, & 
!                     E1 = 0, E2 = 32E-3, FINT = 0, FINTX = 0.5
!
!  BBC2DL2 : LINE = (BBC2DL2_A, BBC2DL2_B)
!
!! bend which closes the dispersion
!
!  BBC2DLF_A : SBEND, TYPE="EXTRACTION BEND", L = 0.5, ANGLE = 4.823E-3, & 
!                     E1 = 4.823E-3, E2 = 0, FINT = 0.5, FINTX = 0
!  BBC2DLF_B : SBEND, TYPE="EXTRACTION BEND", L = 0.5, ANGLE = 4.823E-3, & 
!                     E1 = 0, E2 = 4.823E-3, FINT = 0, FINTX = 0.5
!  bbcdlf : sbend, TYPE="EXTRACTION BEND", l = 1, angle = 0.96463192352641E-02 
!
!  BBC2DLF : LINE = (BBC2DLF_A, BBC2DLF_B) 
!
! ==============================================================================
! Wiggler Bends
! ------------------------------------------------------------------------------

! Define the 8 families of bend magnets.  Note that the bending angles are
! in the configuration files, except for families 1 and 8, which are fixed:  

       BC2B1E1 := -BC2ETAP 
       BC2B1E2 := -BC2B1E1+BC2ANG1 !; VALUE, BC2B1E2
!
       BC2B2E1 := -BC2B1E2 
       BC2B2E2 := -BC2B2E1+BC2ANG2 !; VALUE, BC2B2E2
!
       BC2B3E1 := -BC2B2E2 
       BC2B3E2 := -BC2B3E1+BC2ANG3 !; VALUE, BC2B3E2
!
       BC2B4E1 := -BC2B3E2 
       BC2B4E2 := -BC2B4E1+BC2ANG4 !; VALUE, BC2B4E2
!
       BC2B5E1 := -BC2B4E2 
       BC2B5E2 := -BC2B5E1+BC2ANG5 !; VALUE, BC2B5E2
!
       BC2B6E1 := -BC2B5E2 
       BC2B6E2 := -BC2B6E1+BC2ANG6 !; VALUE, BC2B6E2
!
       BC2B7E1 := -BC2B6E2 
       BC2B7E2 := -BC2B7E1+BC2ANG7 !; VALUE, BC2B7E2
!
       BC2B8E1 := -BC2B7E2 
       BC2B8E2 := -BC2B8E1+BC2ANG8 !; VALUE, BC2B8E2

! The second family is identical to the first but with mirror symmetry:  

  BC2ANG9  := -BC2ANG8 
       BC2B9E1 := -BC2B8E2
       BC2B9E2 := -BC2B9E1 + BC2ANG9    !; VALUE, BC2B9E2
  BC2ANG10 := -BC2ANG7 
       BC2B10E1 := -BC2B9E2  
       BC2B10E2 := -BC2B10E1 + BC2ANG10 !; VALUE, BC2B10E2
  BC2ANG11 := -BC2ANG6 
       BC2B11E1 := -BC2B10E2  
       BC2B11E2 := -BC2B11E1 + BC2ANG11 !; VALUE, BC2B11E2
  BC2ANG12 := -BC2ANG5 
       BC2B12E1 := -BC2B11E2  
       BC2B12E2 := -BC2B12E1 + BC2ANG12 !; VALUE, BC2B12E2
  BC2ANG13 := -BC2ANG4
       BC2B13E1 := -BC2B12E2  
       BC2B13E2 := -BC2B13E1 + BC2ANG13 !; VALUE, BC2B13E2
  BC2ANG14 := -BC2ANG3 
       BC2B14E1 := -BC2B13E2  
       BC2B14E2 := -BC2B14E1 + BC2ANG14 !; VALUE, BC2B14E2
  BC2ANG15 := -BC2ANG2 
       BC2B15E1 := -BC2B14E2  
       BC2B15E2 := -BC2B15E1 + BC2ANG15 !; VALUE, BC2B15E2
  BC2ANG16 := -BC2ANG1 
       BC2B16E1 := -BC2B15E2  
       BC2B16E2 := -BC2B16E1 + BC2ANG16 !; VALUE, BC2B16E2

! now for the magnets themselves, left unsplit in this case for simplicity

  BC2B1 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG1, E1 = BC2B1E1, & 
                 E2 = BC2B1E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B1E1+BC2B1E2+DZ)/(SIN(BC2B1E1)+SIN(BC2B1E2)+DZ)
  BC2B2 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG2, E1 = BC2B2E1, & 
                 E2 = BC2B2E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B2E1+BC2B2E2+DZ)/(SIN(BC2B2E1)+SIN(BC2B2E2)+DZ)
  BC2B3 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG3, E1 = BC2B3E1, & 
                 E2 = BC2B3E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B3E1+BC2B3E2+DZ)/(SIN(BC2B3E1)+SIN(BC2B3E2)+DZ)
  BC2B4 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG4, E1 = BC2B4E1, & 
                 E2 = BC2B4E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B4E1+BC2B4E2+DZ)/(SIN(BC2B4E1)+SIN(BC2B4E2)+DZ)
  BC2B5 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG5, E1 = BC2B5E1, & 
                 E2 = BC2B5E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B5E1+BC2B5E2+DZ)/(SIN(BC2B5E1)+SIN(BC2B5E2)+DZ)
  BC2B6 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG6, E1 = BC2B6E1, & 
                 E2 = BC2B6E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B6E1+BC2B6E2+DZ)/(SIN(BC2B6E1)+SIN(BC2B6E2)+DZ)
  BC2B7 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG7, E1 = BC2B7E1, & 
                 E2 = BC2B7E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B7E1+BC2B7E2+DZ)/(SIN(BC2B7E1)+SIN(BC2B7E2)+DZ)
  BC2B8 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG8, E1 = BC2B8E1, & 
                 E2 = BC2B8E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B8E1+BC2B8E2+DZ)/(SIN(BC2B8E1)+SIN(BC2B8E2)+DZ)

  BC2B9  : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG9, E1 = BC2B9E1, & 
                  E2 = BC2B9E2, HGAP = GB30 / 2,                   & 
          L = LBC2BEND*(BC2B9E1+BC2B9E2+DZ)/(SIN(BC2B9E1)+SIN(BC2B9E2)+DZ)
  BC2B10 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG10, E1 = BC2B10E1, & 
                  E2 = BC2B10E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B10E1+BC2B10E2+DZ)/(SIN(BC2B10E1)+SIN(BC2B10E2)+DZ)
  BC2B11 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG11, E1 = BC2B11E1, & 
                  E2 = BC2B11E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B11E1+BC2B11E2+DZ)/(SIN(BC2B11E1)+SIN(BC2B11E2)+DZ)
  BC2B12 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG12, E1 = BC2B12E1, & 
                  E2 = BC2B12E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B12E1+BC2B12E2+DZ)/(SIN(BC2B12E1)+SIN(BC2B12E2)+DZ)
  BC2B13 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG13, E1 = BC2B13E1, & 
                  E2 = BC2B13E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B13E1+BC2B13E2+DZ)/(SIN(BC2B13E1)+SIN(BC2B13E2)+DZ)
  BC2B14 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG14, E1 = BC2B14E1, & 
                  E2 = BC2B14E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B14E1+BC2B14E2+DZ)/(SIN(BC2B14E1)+SIN(BC2B14E2)+DZ)
  BC2B15 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG15, E1 = BC2B15E1, & 
                  E2 = BC2B15E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B15E1+BC2B15E2+DZ)/(SIN(BC2B15E1)+SIN(BC2B15E2)+DZ)
  BC2B16 : SBEND, TYPE="D25L900V3", ANGLE = BC2ANG16, E1 = BC2B16E1, & 
                  E2 = BC2B16E2, HGAP = GB30 / 2,                    & 
          L = LBC2BEND*(BC2B16E1+BC2B16E2+DZ)/(SIN(BC2B16E1)+SIN(BC2B16E2)+DZ)

! ==============================================================================
! Quads
! ------------------------------------------------------------------------------

! Because of the extra drift every 4 RF units (ie, every 2 FODO cells), the
! lattice is matched to have 120 degree phase advance and perfect periodicity
! every 2 cells.

  KQBC2CM := 0.129806038121 !0.039139261011 * 3.33

  QDBC2CM : QCM, K1 = -KQBC2CM         
  QFBC2CM : QCM, K1 =  KQBC2CM        

! matching quads:  upstream end

  QMATCH1CM : QCM, K1 = KQM1BC2
  QMATCH2CM : QCM, K1 = KQM2BC2
  QMATCH3CM : QCM, K1 = KQM3BC2
  QMATCH4CM : QCM, K1 = KQM4BC2
  QMATCH5CM : QCM, K1 = KQM5BC2

! matching quads:  downstream end

  QMATCH6CM : QCM, K1 = KQM6BC2
  QMATCH7CM : QCM, K1 = KQM7BC2
  QMATCH8CM : QCM, K1 = KQM8BC2
  QMATCH9CM : QCM, K1 = KQM9BC2

! warm quads between the RF and the wiggler

  QFBC2M1 : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = rQ7, K1 = KQFBC2M1
  QDBC2M1 : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = rQ7, K1 = KQDBC2M1
  QFBC2M2 : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = rQ7, K1 = KQFBC2M2
  QDBC2M2 : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = rQ7, K1 = KQDBC2M2

! Quads for the wiggler

!CORRECTION  
!  QFBC2W : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = RQ7, K1 = KQFBC2W
!  QDBC2W : QUAD, TYPE="Q20L200", L = LQ7/2, APERTURE = RQ7, K1 = KQDBC2W
  QFBC2W : QUAD, TYPE="Q20L200", L = LQ7/2/COS(BC2B1E1), APERTURE = RQ7, &        
                 K1 = KQFBC2W        
  QDBC2W : QUAD, TYPE="Q20L200", L = LQ7/2/COS(BC2B8E2), APERTURE = RQ7, &        
                 K1 = KQDBC2W        


! Tuning quads in the wiggler

!CORRECTION  
!  CQBC2_1 : QBCTUNE, K1 =  KCQBC2_12
!  CQBC2_2 : QBCTUNE, K1 = -KCQBC2_12
!  CQBC2_3 : QBCTUNE, K1 =  KCQBC2_34
!  CQBC2_4 : QBCTUNE, K1 = -KCQBC2_34
!
!  SQBC2_1 : QBCTUNE, TILT
!  SQBC2_2 : QBCTUNE, TILT
!  SQBC2_3 : QBCTUNE, TILT
!  SQBC2_4 : QBCTUNE, TILT

  QBC2TUNE12 : QUAD, TYPE="Q50L100", L = LZCQBC/2/COS(BC2B1E2), &          
                    APERTURE = ACQBC/2        
  QBC2TUNE78 : QUAD, TYPE="Q50L100", L = LZCQBC/2/COS(BC2B7E2), &        
                    APERTURE = ACQBC/2         
         
  CQBC2_1 : QBC2TUNE78, K1 =  KCQBC2_12         
  CQBC2_2 : QBC2TUNE78, K1 = -KCQBC2_12         
  CQBC2_3 : QBC2TUNE78, K1 =  KCQBC2_34        
  CQBC2_4 : QBC2TUNE78, K1 = -KCQBC2_34        
         
  SQBC2_1 : QBC2TUNE12, TILT        
  SQBC2_2 : QBC2TUNE12, TILT        
  SQBC2_3 : QBC2TUNE12, TILT        
  SQBC2_4 : QBC2TUNE12, TILT        


! Match to the diagnostic section:  since the wiggler lattice changes
! slightly when the configuration is varied, the strengths of these 
! quads are technically part of the configuration

  QDBC2W2D1 : QUAD, TYPE="Q20L200", L = LQ7 / 2, APERTURE = RQ7, & 
                    K1 = KQDBC2W2D1
  QFBC2W2D1 : QUAD, TYPE="Q20L200", L = LQ7 / 2, APERTURE = RQ7, & 
                    K1 = KQFBC2W2D1
  QDBC2W2D2 : QUAD, TYPE="Q20L200", L = LQ7 / 2, APERTURE = RQ7, & 
                    K1 = KQDBC2W2D2
  QFBC2W2D2 : QUAD, TYPE="Q20L200", L = LQ7 / 2, APERTURE = RQ7, & 
                    K1 = KQFBC2W2D2

! Diagnostic section quads; pole field is about 0.5 T

  KQBC2D := 0.10534444625899E+01        
  QFBC2D : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, & 
                 K1 =  KQBC2D
  QDBC2D : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, & 
                 K1 = -KQBC2D

! Matching to the main linac:  

  QMBCMLF1 : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, K1 = KQMBCMLF1p
  QMBCMLD2 : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, K1 = KQMBCMLD2p
  QMBCMLF3 : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, K1 = KQMBCMLF3p

! this quad is part of the separation process for the extracted beam, 
! so it has to have a fixed strength; also it needs a large aperture
! to pass the extracted beam cleanly.

  QMBCMLDFIX : QUAD, TYPE="Q60L200", L = LQ7/2, APER = 0.03, & 
                     K1 = -0.37113053450385E+00

! back to quads that can be used for matching

  QMBCMLF5 : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, K1 = KQMBCMLF5p
  QMBCMLD6 : QUAD, TYPE="Q20L200", L = LQ7/2, APER = RQ7, K1 = KQMBCMLD6p

! Quads in the extraction channel
!
!  QFBCDL1 : QUAD, TYPE="QUAD_DUMPLINE", L = 1/2, APER = 0.03, & 
!                  K1 =  0.35989384145058E+00
!  QDBCDL1 : QUAD, TYPE="QUAD_DUMPLINE", L = 1/2, APER = 0.03, & 
!                  K1 = -0.35849333704904E+00
!  QFBCDL2 : QUAD, TYPE="QUAD_DUMPLINE", L = 1/2, APER = 0.03, & 
!                  K1 =  0.52286416116853E+00
!
!  QFBCDL3 : QFBCDL1, k1 = 0.58924590541867E+00
!  QDBCDL3 : QDBCDL1, APER = 1.5E-02, k1 = -0.82835016007285E+00
!  QfBCDL4 : QDBCDL1, K1 = 0.5
!  QFBCDL5 : QUAD, TYPE="QUAD_DUMPLINE", L = 1/2, APER = 0.03, & 
!                  K1 =  0.29562963143795E+00
!  QDBCDL5 : QUAD, TYPE="QUAD_DUMPLINE", L = 1/2, APER = 0.03, & 
!                  K1 = -0.34418369491926E+00

! ==============================================================================
! Collimators
! ------------------------------------------------------------------------------

! Energy spoiler is set to collimate at about 6 sigma with 1.5% energy
! spread at a location with 4.5 cm dispersion

  SPBC2 : RCOLL, YSIZE = 1E-2, XSIZE = 4E-3

! fixed absorber for off-energy particles

  ABBC2 : ECOLL, YSIZE = 5E-3, XSIZE = 5E-3

! ==============================================================================
! Emergency extraction kickers
! ------------------------------------------------------------------------------

! There are 10 of these guys, so:

  HKEXT_BC2 : HKICK, TYPE="KICKER", L=1.0, KICK=-BC2_ABORT_ANGLE/10

! ==============================================================================
! Drifts
! ------------------------------------------------------------------------------

! Drifts within the matcher and suppressor:

  DBC2MB1 : DRIFT, L = 2.599  !1.0; 
  DBC2MB2 : DRIFT, L = 12.659 !22.4 ; 
  DBC2MB3 : DRIFT, L = 10.034 !8.4; 
  DBC2MB4 : DRIFT, L = 5.034  !9.2 ; 
  DBC2MB5 : DRIFT, L = 2.979  !5.0 ; 

  DBC2_4 : DRIFT, L = 1.1000968 
  DBC2M  : DRIFT, L = LBC2M / 2

! Drifts within the fixed geometry part of the wiggler

!CORRECTION
!  DBC_CQ : DRIFT, L = QBCTUNE[L]
  DBC2_CQ : DRIFT, L = LZCQBC/2/COS(BC2B1E2)
  DBC2_SQ : DRIFT, L = LZCQBC/2/COS(BC2B7E2)

!CORRECTION    
!  DBC2_01      : DRIFT, L = 0.3
!  DBC2_BCQ12   : DRIFT, L = 0.3
!  DBC2_BPM12   : DRIFT, L = 0.1 / 2
  DBC2_01    : DRIFT, L = 0.3 / COS(BC2B1E1)                     
  DBC2_BCQ12 : DRIFT, L = 0.3 / COS(BC2B1E2)                      
  DBC2_BPM12 : DRIFT, L = 0.1 / 2 / COS(BC2B1E2)                     

!CORRECTION     
!  DBC2_BCQ78   : DRIFT, L = 0.3
!  DBC2_BPM78   : DRIFT, L = 0.1 / 2
!  DBC2_80      : DRIFT, L = 0.3
  DBC2_BCQ78 : DRIFT, L = 0.3 / COS(BC2B7E2)                
  DBC2_BPM78 : DRIFT, L = 0.1 / 2 / COS(BC2B7E2)                  
  DBC2_80    : DRIFT, L = 0.3 / COS(BC2B8E2)                

!CORRECTION   
!  DBC2_09      : DRIFT, L = 0.3
!  DBC2_BCQ910  : DRIFT, L = 0.3
!  DBC2_BPM910  : DRIFT, L = 0.1 / 2
  DBC2_09     : DRIFT, L = 0.3 / COS(BC2B8E2)                
  DBC2_BCQ910 : DRIFT, L = 0.3 / COS(BC2B9E2)                 
  DBC2_BPM910 : DRIFT, L = 0.1 / 2 / COS(BC2B9E2)         

!CORRECTION    
!  DBC2_BCQ1516 : DRIFT, L = 0.3
!  DBC2_BPM1516 : DRIFT, L = 0.1 / 2
!  DBC2_160     : DRIFT, L = 0.3
  DBC2_BCQ1516 : DRIFT, L = 0.3 / COS(BC2B15E2)                   
  DBC2_BPM1516 : DRIFT, L = 0.1 / 2 / COS(BC2B15E2)                
  DBC2_160     : DRIFT, L = 0.3 / COS(BC2ETAP)                  

! Drifts within the wiggler have 30 cm length in the z direction, but their
! path-length contributions are variable

  DBC2_23 : DRIFT, L = 0.3 / COS(BC2B2E2)
  DBC2_34 : DRIFT, L = 0.3 / COS(BC2B3E2)
  DBC2_45 : DRIFT, L = 0.3 / COS(BC2B4E2)
  DBC2_56 : DRIFT, L = 0.3 / COS(BC2B5E2)
  DBC2_67 : DRIFT, L = 0.3 / COS(BC2B6E2)

  DBC2_1011 : DRIFT, L = 0.3 / COS(BC2B10E2)
  DBC2_1112 : DRIFT, L = 0.3 / COS(BC2B11E2)
  DBC2_1213 : DRIFT, L = 0.3 / COS(BC2B12E2)
  DBC2_1314 : DRIFT, L = 0.3 / COS(BC2B13E2)
  DBC2_1415 : DRIFT, L = 0.3 / COS(BC2B14E2)

  COMMENT !check for symmetric bend-to-bend drift lengths in wiggler cells
    VALUE, DBC2_SQ[L],DBC2_CQ[L]
    VALUE, DBC2_BCQ12[L]+2*DBC2_BPM12[L]+ &
      2*SQBC2_1[L]+DBC2_BCQ12[L]
    VALUE, DBC2_BCQ78[L]+DBC2_BPM78[L]+DBC2_BPM78[L]+ &
      2*CQBC2_1[L]+DBC2_BCQ78[L]
    VALUE, DBC2_BCQ12[L]+2*DBC2_BPM12[L]+ &
      2*DBC2_SQ[L]+DBC2_BCQ12[L]
    VALUE, DBC2_BCQ78[L]+DBC2_BPM78[L]+DBC2_BPM78[L]+ &
      2*DBC2_CQ[L]+DBC2_BCQ78[L]
    VALUE, DBC2_BCQ910[L]+2*DBC2_BPM910[L]+ &
      DBC2_SQ[L]+SPBC2[L]+DBC2_SQ[L]+DBC2_BCQ910[L]
    VALUE, DBC2_BCQ1516[L]+2*DBC2_BPM1516[L]+ &
      DBC2_CQ[L]+ABBC2[L]+DBC2_CQ[L]+DBC2_BCQ1516[L]
  ENDCOMMENT

! the drifts around the tuning quads are 30 cm path length, and there is a
! BPM with 10 cm path length

  DBC2_BCQ : DRIFT, L = 0.3
  DBC2_BPM : DRIFT, L = 0.1 / 2

! Drifts in the match from wiggler to diagnostic station

  DBC2W2D1 : DRIFT, L = 4.3
  DBC2W2D2 : DRIFT, L = 3
  DBC2W2D3 : DRIFT, L = 3
  DBC2W2D4 : DRIFT, L = 3
  DBC2W2D5 : DRIFT, L = 3

! Diagnostic section standard drift

  DBC2D : DRIFT, L = 3.5

! shortened drift to allow room for a wire scanner

  DBC2Ds : DRIFT, L = DBC2D[L] - DR30CM[L]

! shortened even further to allow room for a LOLA or a profile monitor

  DBC2Dss1 : DRIFT, L = DBC2Ds[L] - DR30CM[L] - LOLABC[L]
  DBC2Dss2 : DRIFT, L = DBC2Ds[L] - DR30CM[L]
  DBC2Dss3 : DRIFT, L = DBC2Ds[L] - 2*DR30CM[L] - LOLABC[L]
  DBC2Dss4 : DRIFT, L = DBC2Ds[L] - 2*DR30CM[L] 

! Matching section drifts

  DM1BCML   : DRIFT, L = 2 - DR30CM[L] 
  DM2BCML   : DRIFT, L = 4
  DM3BCML   : DRIFT, L = 8
  DM3BCMLs  : DRIFT, L = DM3BCML[L]   &
                       - 2*BBC2CH1[L] &
                       - 2*BBC2CH2[L] &
                       - 3*DR30CM[L]
  DM3BCMLss : DRIFT, L = DM3BCMLs[L]/2
  DBC2KIK   : DRIFT, L = 15
  DBC2KIKs  : DRIFT, L = DBC2KIK[L] &
                       - 10 * DR30CM[L] &
                       - 10 * HKEXT_BC2[L] &
                       - 2 * BEXT_BC2[L]
  DBC2KIKss : DRIFT, L = DBC2KIKs[L] / 2
  DBC2SEP   : DRIFT, L = 15
  DBC2SEP2  : DRIFT, L = 12*BC2SEPT_A[L] + 6*DR30CM[L]
  DBC2SEP1  : DRIFT, L = DBC2SEP[L] - DBC2SEP2[L]
  DBC2SEPs  : DRIFT, L = DBC2SEP[L] / 2
  DBC2BEND  : DRIFT, L = 10
  DM4BCML   : DRIFT, L = 4

! Extraction channel drifts

!  DBCDL1 : DRIFT, L = 0.5
!
!  DBCDL2 : DRIFT, L = 1
!  DBCDL3 : DRIFT, L = 1
!
!  DBCDL4 : DRIFT, L = 20
!  dbcdl6 : drift, l = 3.6
!  dbcdlx : drift, l = 0.10833900192269E+02

! ==============================================================================
! Location of the dump
! ------------------------------------------------------------------------------

  BC2DUMP : MARK

! ==============================================================================
! Markers
! ------------------------------------------------------------------------------

  BEGINPBC2       : MARKER
  ENDPBC2         : MARKER
  BEGINPLINLAUNCH : MARKER
  ENDPLINLAUNCH   : MARKER
  BEGINPBC2DL     : MARKER
  ENDPBC2DL       : MARKER
  BC2YGIRDER      : MARKER
                                           
  BEGINBC2WIG     : MARKER                     

  BEGPBC2D : MARK

! ==============================================================================
! Lines
! ------------------------------------------------------------------------------

! Match from the BC2 rf section to the wiggler

  BC2RF2W : LINE = (DBC2MB1,                                 &
                    2*QFBC2M1, BPMQ079, XCOR, YCOR, DBC2MB2, &
                    2*QDBC2M1, BPMQ079,       YCOR, DBC2MB3, &
                    2*QFBC2M2, BPMQ079, XCOR, YCOR, DBC2MB4, &
                    2*QDBC2M2, BPMQ079,       YCOR, DBC2MB5, &
                    DBC2M                                    )

! wiggler in the first half-cell, including slots for tuning quads

  BC2WIG1(CQ,SQ) : LINE = (DBC2_01,                                   &
                           BC2B1, DBC2_BCQ12,                         &
                           2*DBC2_BPM12, 2*SQ, DBC2_BCQ12,            &
                           BC2B2, DBC2_23,                            &
                           BC2B3, DBC2_34,                            &
                           BC2B4, DBC2_45,                            &
                           BC2B5, DBC2_56,                            &
                           BC2B6, DBC2_67,                            &
                           BC2B7, DBC2_BCQ78,                         &
                    DBC2_BPM78, BPMETA, DBC2_BPM78, 2*CQ, DBC2_BCQ78, &
                           BC2B8, DBC2_80                               )


! wiggler in the second half cell, including occasional collimators  

  BC2WIG2(C1,C2) : LINE = (DBC2_09,                                    & 
                           BC2B9, DBC2_BCQ910,                         &
                     2*DBC2_BPM910, DBC2_SQ, C1, DBC2_SQ, DBC2_BCQ910, &
                           BC2B10, DBC2_1011,                          &
                           BC2B11, DBC2_1112,                          &
                           BC2B12, DBC2_1213,                          &
                           BC2B13, DBC2_1314,                          &
                           BC2B14, DBC2_1415,                          &
                           BC2B15, DBC2_BCQ1516,                       &
                   2*DBC2_BPM1516, DBC2_CQ, C2, DBC2_CQ, DBC2_BCQ1516, &
                           BC2B16, DBC2_160                              )

! Standard wiggler cell is from QF to QF and contains 2 wiggler modules

! Complete BC2 wiggler -- 6 cells plus matcher and suppressor

  BC2WIGGLER : LINE = (BEGINBC2WIG,                                    &
                       BC2MB1, DBC2M, DR30CM, DBC2M, BC2MB2,           &
                       DBC2M, DR30CM, DBC2M, BC2MB3, DBC2M, DBC2_4,    &
                       QFBC2W, BC2MATCH2, QFBC2W, BPMQ079, XCOR, YCOR, &
                       BC2WIG1(CQBC2_1,SQBC2_1),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(SPBC2,ABBC2),                           &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       BC2WIG1(DBC2_CQ,DBC2_SQ),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(NOCOLL,NOCOLL),                         &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       BC2WIG1(CQBC2_2,SQBC2_2),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(SPBC2,ABBC2),                           &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       BC2WIG1(CQBC2_3,SQBC2_3),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(NOCOLL,NOCOLL),                         &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       BC2WIG1(DBC2_CQ,DBC2_SQ),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(NOCOLL,NOCOLL),                         &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       BC2WIG1(CQBC2_4,SQBC2_4),                       &
                       2*QDBC2W, BPMQ079,       YCOR,                  &
                       BC2WIG2(NOCOLL,NOCOLL),                         &
                       2*QFBC2W, BPMQ079, XCOR, YCOR,                  &
                       DBC2_4, DBC2M, BC2MB4, DBC2M, DR30CM, DBC2M,    &
                       BC2MB5, DBC2M, DR30CM, DBC2M, BC2MB6              )

! Match from the wiggler to the diagnostic station

  BC2W2D : LINE = (DBC2M,                            DBC2W2D1, &
                   2*QDBC2W2D1, BPMQ079,       YCOR, DBC2W2D2, &
                   2*QFBC2W2D1, BPMQ079, XCOR, YCOR, DBC2W2D3, &
                   2*QDBC2W2D2, BPMQ079,       YCOR, DBC2W2D4, &
                   2*QFBC2W2D2, BPMQ079, XCOR, YCOR, DBC2W2D5  )

! Initial Twiss of the BC2DIAG line:

  BC2DIAG_TWISS : BETA0, BETX =  6.5526, ALFX =  0.7032, &
                         BETY = 14.2693, ALFY = -1.5015

! Note:  this system contains 4 short S-band deflecting structures.  The
! first and last are vertical deflectors, with a -I between them so that 
! it can (hopefully) be arranged that their kicks cancel.  The middle pair
! are horizontal deflectors with 90 degree phase advance between them.  
! The point of this is that the first cavity "streaks" in y for pure bunch
! length measurements, or E vs z measurements in the extraction line (where
! there is horizontal dispersion); the middle cavities "streak" in x so
! that any y vs z structure in either plane is seen; and the last cavity
! produces a "streak" equal and opposite to the first cavity so that in 
! principle the bunch length part of the system could possibly be used in
! a non-invasive way, although this would require a different type of readout
! than is currently provided. 

  BC2DIAG : LINE = (2*QDBC2D, BPMQ079,       YCOR, DR30CM, WS,         &
                    DR30CM, PHASMON, LOLABC, DR30CM,         DBC2Dss3, &
                    2*QFBC2D, BPMQ079, XCOR, YCOR, DBC2D,              &
                    2*QDBC2D, BPMQ079,       YCOR, DR30CM, WS, DBC2Ds, &
                    2*QFBC2D, BPMQ079, XCOR, YCOR, DBC2D,              &
                    2*QDBC2D, BPMQ079,       YCOR, DR30CM, WS,         &
                DR30CM, BLMO_HORN, LOLAPROF, DR30CM,         DBC2Dss4, &
                    2*QFBC2D, BPMQ079, XCOR, YCOR, DBC2D,              &
                    2*QDBC2D, BPMQ079,       YCOR, DR30CM, WS, DBC2Ds, &
                    2*QFBC2D, BPMQ079, XCOR, YCOR, DBC2D,              &
                    2*QDBC2D, BPMQ079,       YCOR, DR30CM,             &
                LOLAPROF, DR30CM )

! matching up to the upstream face of the first septum bend in the
! extraction channel

  BC2_ML_1 : LINE = (                                 DM1BCML,   &
                     2*QMBCMLF1, BPMQ079, XCOR, YCOR, DM2BCML,   &
                     2*QMBCMLD2, BPMQ079,       YCOR, DM3BCMLss, &
                     BBC2CH1, DR30CM, BBC2CH2, DR30CM,           &
                     BBC2CH2, DR30CM, BBC2CH1, DM3BCMLss,        & 
                     2*QMBCMLF3, BPMQ079, XCOR, YCOR,            &
!
! kickers and pulsed bend
!
                     DBC2KIKss,                                &
                     BEGPBC2D,                                 &
                     5*(HKEXT_BC2, DR30CM),                    &
                     2*BEXT_BC2,                               &
                     5*(DR30CM, HKEXT_BC2),                    &
                     DBC2KIKss,                                &
!
! back to quads -- DBC2SEP1 is a divergent chamber
!
                     2*QMBCMLDFIX, BPMQ079,     YCOR, DBC2SEP1, &
                     BC2YGIRDER                                     )

! line downstream of the upstream face of the septum bend

  BC2_ML_2 : LINE = (DBC2SEP2, &
                     2*QMBCMLF5, BPMQ079, XCOR, YCOR, DBC2BEND, &
                     2*QMBCMLD6, BPMQ079,       YCOR,           &
                     DM4BCML                                        )
!                     BC2PPS1, BC2PPS2, DM4BCML, BC2PPS3            )

! extraction line, including Twiss parameters

!  BC2_DL : LINE = (BEGINPBC2DL,                                &
!                   BC2DL_NEWAXIS, 
!                               6*(BC2SEPT, DR30CM), DBCDL1,    &
!
! focusing doublet
!
!                   2*QFBCDL1, BPMDL, XCOR, YCOR, DBCDL1, &
!                   2*QDBCDL1, BPMDL, XCOR, YCOR, DR30CM, &
!
! bend magnet
!
!                   BBC2DL1, dr30cm, &
!
! quad at center of bending region
!
!                   2*QFBCDL2, BPMDL, XCOR, YCOR, DR30CM, &
!
! another bend magnet
!
!                   BBC2DL2, DR30CM, &
!
! focusing triplet
!
!                   2*QFBCDL3, BPMDL, XCOR, YCOR, DBCDL1, &
!                   2*QDBCDL3, BPMDL, XCOR, YCOR, DBCDL1, &
!                   2*QFBCDL3, BPMDL, XCOR, YCOR, DR30CM, &
!
! another bend magnet with synchrotron light port and placeholder
! profile monitor marker for looking at the streaked bunch from
! LOLA
!
!                   BBC2DL2, DR30CM, LOLASYNC, LOLAPROF, &
!
! focusing triplet 
!
!                   2*QFBCDL4, BPMDL, XCOR, YCOR, Dbcdl6, &
!                   2*QdBCDL5, BPMDL, XCOR, YCOR, DBCDL3, & 
!                   2*QfBCDL5, BPMDL, XCOR, YCOR, dbcdlx, &
!
! final bend to zero dispersion and drift to the dump
!
!                   BBC2DLF, XRASTER, YRASTER, dbcdl4, bc2dump, &
!                   ENDPBC2DL )

! full straight-ahead system

  ENDBC2RF : MARK
  PBC2 : LINE = (BEGINPBC2, BC2RF, ENDBC2RF, BC2RF2W, &
                 BC2WIGGLER, BC2W2D, ENDPBC2, &
                 BEGINPLINLAUNCH, BC2DIAG, BC2_ML_1, BC2_ML_2, ENDPLINLAUNCH)

! ------------------------------------------------------------------------------
! temporary beamlines for matching
! ------------------------------------------------------------------------------

  BC2CAV0 : DRIF, L=BC2CAV[L]

  PBC2a : LINE=(&
    CM(BC2CAV0),DMM,CMDQUAD(QDBC2CM,BC2CAV0),DMM,CM(BC2CAV0),DMM,&
    CM(BC2CAV0),DMM,CMFQUAD(QFBC2CM,BC2CAV0),DMM,CM(BC2CAV0),DMM,&
    CM(BC2CAV0),DMM,CMDQUAD(QDBC2CM,BC2CAV0),DMM,CM(BC2CAV0),DMM,&
    CM(BC2CAV0),DMM,CMFQUAD(QFBC2CM,BC2CAV0),DMM,CM(BC2CAV0),DMM,DENDBOX)

  PBC2b : LINE=(&
      QFBC2W,BPMQ079,XCOR,YCOR,BC2WIG1(DBC2_CQ,DBC2_SQ),&
    2*QDBC2W,BPMQ079,     YCOR,BC2WIG2(NOCOLL,NOCOLL),&
      QFBC2W)

  PBC2c : LINE=(&
    2*QDBC2D,BPMQ079,YCOR,DR30CM,WS,DR30CM,PHASMON,LOLABC,DR30CM,DBC2Dss3,&
    2*QFBC2D,BPMQ079,XCOR,YCOR,DBC2D)

! ------------------------------------------------------------------------------

RETURN    

!==============================================================================
! subsystem=pmpscol
! ------------------------------------------------------------------------------
! Prerequisite files ($ILC = ILC lattice root directory):
!   $ILC/parameters/ilc000.ILC_common.xsif"
!   $ILC/parameters/ilc008.BDS_common.xsif"
! ------------------------------------------------------------------------------
! 15-Apr-2015, M. Woodley (SLAC)
!    ILC2015a release
! 22-AUG-2014, M. Woodley [SLAC]
!    From ilc541.empscol.xsif
! ------------------------------------------------------------------------------

! input Twiss

  PMLENDBETX := 72.467317526897 !twiss beta x (m)
  PMLENDALFX := -1.543862237225 !twiss alpha x
  PMLENDBETY := 36.223716658488 !twiss beta y (m)
  PMLENDALFY := 1.105588583522  !twiss alpha y

! ==============================================================================
! QUADs
! ------------------------------------------------------------------------------

! matching from PML

  BQM1MPSC :=  6.673098307447*Efact
  BQM2MPSC := -6.608248889802*Efact

  QM1MPSC : QBDS4, K1=BQM1MPSC*QFACT4
  QM2MPSC : QBDS4, K1=BQM2MPSC*QFACT4

! 90 deg collimation FODO cells

  BQFMPSC  :=  9.536306146579*Efact
  BQDMPSC  := -BQFMPSC

  QFMPSC  : QBDS4, K1=BQFMPSC*QFACT4
  QDMPSC  : QBDS4, K1=BQDMPSC*QFACT4

! ==============================================================================
! DRIFTs
! ------------------------------------------------------------------------------

  LCMPSC  := 3.0                 !collimator length
  LMLMPSC := 0.2                 !cryo service box end to 1st quadrupole
  LQQMPSC := 0.15                !half drift between quadrupole pairs
  LM1MPSC := 15.420718847019     !linac-to-MPS matching drift
  LM2MPSC := 12.090789243414     !linac-to-MPS matching drift
  LCQMPSC := 0.5                 !collimator to quadrupole drift
  LQCMPSC := 12.5-LCQMPSC-LCMPSC !quadrupole to collimator drift

  dLMPSC := 0

  DMLMPSC : DRIF, L=LMLMPSC
  DQQMPSC : DRIF, L=LQQMPSC
  DM1MPSC : DRIF, L=LM1MPSC+dLMPSC
  DM2MPSC : DRIF, L=LM2MPSC-dLMPSC
  DCQMPSC : DRIF, L=LCQMPSC
  DQCMPSC : DRIF, L=LQCMPSC

  LPADJ1 := 0.462314587681E-3 !tiny tweak to get close to TPML2BDS coordinates

  DPADJ1 : DRIF, L=LPADJ1

  VALUE, 
! ==============================================================================
! collimators
! ------------------------------------------------------------------------------

  XCMPSC1 := 25e-3
  XCMPSC2 := 15e-3
  XCMPSC3 :=  6e-3

  YCMPSC1 := 25e-3
  YCMPSC2 := 15e-3
  YCMPSC3 :=  6e-3

  CMPSC1 : ECOL, L=LCMPSC, XSIZE=XCMPSC1, YSIZE=YCMPSC1
  CMPSC2 : ECOL, L=LCMPSC, XSIZE=XCMPSC2, YSIZE=YCMPSC2
  CMPSC3 : ECOL, L=LCMPSC, XSIZE=XCMPSC3, YSIZE=YCMPSC3

! ==============================================================================
! MARKs
! ------------------------------------------------------------------------------

  BEG_PMPSCOL : MARK
  MMPSCOL     : MARK
  END_PMPSCOL : MARK

! ==============================================================================
! LINEs
! ------------------------------------------------------------------------------

  MPSCFODO : LINE=(&
    QFMPSC,BPMQ315,MMOVER,QFMPSC,2*DQQMPSC,&
    QFMPSC,BPMQ315,MMOVER,QFMPSC,DQCMPSC,CMPSC2,DCQMPSC,&
    QDMPSC,BPMQ315,MMOVER,QDMPSC,2*DQQMPSC,&
    QDMPSC,BPMQ315,MMOVER,QDMPSC,DQCMPSC,CMPSC3,DCQMPSC)

  PMPSCOL : LINE=(BEG_PMPSCOL,&
    DMLMPSC,&
    QM1MPSC,BPMQ315,MMOVER,QM1MPSC,2*DQQMPSC,&
    QM1MPSC,BPMQ315,MMOVER,QM1MPSC,DM1MPSC,&
    QM2MPSC,BPMQ315,MMOVER,QM2MPSC,2*DQQMPSC,&
    QM2MPSC,BPMQ315,MMOVER,QM2MPSC,DM2MPSC,CMPSC1,DCQMPSC,MMPSCOL,&
    QFMPSC,BPMQ315,MMOVER,QFMPSC,2*DQQMPSC,&
    QFMPSC,BPMQ315,MMOVER,QFMPSC,DQCMPSC,CMPSC2,DCQMPSC,&
    QDMPSC,BPMQ315,MMOVER,QDMPSC,2*DQQMPSC,&
    QDMPSC,BPMQ315,MMOVER,QDMPSC,DQCMPSC,CMPSC3,DCQMPSC,&
    DPADJ1,&
    END_PMPSCOL)

! ------------------------------------------------------------------------------

  RETURN

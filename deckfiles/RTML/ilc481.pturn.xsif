!========1=========2=========3=========4=========5=========6=========7=========8
!
! ILC RTML e+ turnaround, PTURN.XSIF
!
! New version:  compared to the original turnaround, here are the changes:
!   1.  Doubled the cell length 
!   2.  Increased the phase advance per cell to 108 degrees
!   3.  Inserted doglegs to change the position of the RTML
!       from near-ceiling in the long transfer line to linac level in
!       the forward RTML, and from the center of the tunnel to the linac
!       line in x.  The doglegs have 90 degree phase advance per cell.
!   4.  Exchanged the relationship of the short and long arcs, with the long
!       arc now first.  This is set by the horizontal alignments of the 
!       long transfer line and the linac in the main linac tunnel.
!   5.  Elimination of the tuning quads:  horizontal dispersion can be
!       tuned by using two standard arc quads, while vertical dispersion
!       can be tuned by using 2 vertical dogleg quads.
!   6.  Eliminate xcors at D quads.  Move BPMs to downstream of quads.
!
! This lattice requires the following files to run properly:
!
!    RTML_COMMON.XSIF
!
! plus one of the following (mainly for bunch charge):
!
!    RTML_sigznominal.xsif
!    RTML_sigzLowN.xsif
!
! Auth:  PT, 26-Oct-2006.
! 
! Mod:
!   15-Apr-2015, M. Woodley (SLAC)
!      ILC2015a release
!   24-Feb-2012, AV
!      Changed angles AV1, AV2, AH1, AH2 to match treaty points.
!   23-Apr-2007, PT
!      New values of matching quads for shorter spin rotator solenoids.
!   10-APR-2007, PT
!      Added BSIGN to support electron and positron geometries.
!   12-jan-2007, PT
!      Added spoilers and absorbers to horizontal dogleg.
! ------------------------------------------------------------------------------

! Initial Twiss and Beam statements

! here's the TWISS for the nominal entry point

  TWISS_PTURN : BETA0, BETX = 10.9294, ALFX =  3.0356, &
                       BETY =  1.2617, ALFY = -0.4263, &
                       DX = 0, DPX = 0, DY = 0, DPY = 0

  BEAM_PTURN  : BEAM, ENERGY = 5, NPART = BunchCharge, &
                      SIGE = 1.5E-3, SIGT = 6.E-3, &
                      EX = 8E-6 * EMASS/5, EY = 20E-9 * EMASS/5

! defined parameters

  LQA := 0.20  !arc quad effective length (m)

! ==============================================================================
! BENDs
! ------------------------------------------------------------------------------

! Turnaround arc bends

! VK&NS 20111020
!  ABD1 := -BSIGN * 0.41348161915782E-01
!  ABD2 := -BSIGN * 0.67582405620248E-01
!  ABD3 := -BSIGN * 0.67254219563421E-01
!  ABD4 := -BSIGN * 0.41402011892442E-01
!  ABA  := -BSIGN * 0.10933421599710

  ABD1 := +BSIGN * 0.41348161915782E-01
  ABD2 := +BSIGN * 0.67582405620248E-01
  ABD3 := +BSIGN * 0.67254219563421E-01
  ABD4 := +BSIGN * 0.41402011892442E-01
  ABA  := +BSIGN * 0.10933421599710

  ABD1R := -ABD1
  ABD2R := -ABD2
  ABD3R := -ABD3
  ABD4R := -ABD4
  ABAR  := -ABA 

! "forward" arc bends

  BAF  : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABA,  &
                E1=ABA/2,  E2=ABA/2
  BD1F : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD1, &
                E1=ABD1/2, E2=ABD1/2
  BD2F : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD2, &
                E1=ABD2/2, E2=ABD2/2
  BD3F : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD3, &
                E1=ABD3/2, E2=ABD3/2
  BD4F : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD4, &
                E1=ABD4/2, E2=ABD4/2

! "reverse" arc bends

  BAR  : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABAR,  &
                E1=ABAR/2,  E2=ABAR/2
  BD1R : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD1R, &
                E1=ABD1R/2, E2=ABD1R/2
  BD2R : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD2R, &
                E1=ABD2R/2, E2=ABD2R/2
  BD3R : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD3R, &
                E1=ABD3R/2, E2=ABD3R/2
  BD4R : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=ABD4R, &
                E1=ABD4R/2, E2=ABD4R/2

! Horizontal dogleg bends

! VK&NS 20111020
!  AH1 := -BSIGN * 0.004029
!  AH2 := -BSIGN * 0.004029

 !fHDOG := 1.1270197985601 !earlier
 !fHDOG := 1.1270198158690 !ILC2012e_AV
  fHDOG := 1.022163175854  !ILC2015a

  AH1 := -BSIGN * 0.004029*(-1.45/1.07)*0.98*fHDOG
  AH2 := -BSIGN * 0.004029*(-1.45/1.07)*0.98*fHDOG

  BH1 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE= AH1, &
               E1= AH1/2, E2= AH1/2
  BH2 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE= AH2, &
               E1= AH2/2, E2= AH2/2
  BH3 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=-AH1, &
               E1=-AH1/2, E2=-AH1/2
  BH4 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE=-AH2, &
               E1=-AH2/2, E2=-AH2/2

! Vertical dogleg bends 

! VK&NS 20111020
!  AV1 := 0.011169
!  AV2 := 0.011169

 !fVDOG := 1.0044718580198 !earlier
 !fVDOG := 1.0044461029887 !ILC2012e_AV
  fVDOG := 0.998320763922  !ILC2015a

  AV1 := +0.011169*(1.65/2.14)*fVDOG
  AV2 := +0.011169*(1.65/2.14)*fVDOG

  BV1 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, TILT, ANGLE= AV1, &
               E1= AV1/2, E2= AV1/2
  BV2 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, TILT, ANGLE= AV2, &
               E1= AV2/2, E2= AV2/2
  BV3 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, TILT, ANGLE=-AV1, &
               E1=-AV1/2, E2=-AV1/2
  BV4 : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, TILT, ANGLE=-AV2, &
               E1=-AV2/2, E2=-AV2/2

! ==============================================================================
! QUADs
! ------------------------------------------------------------------------------

! arc FODO cells

  KQDA := -2.733303734086 !-0.27335934255751E+01
  KQFA :=  2.759920467878 ! 0.27599525631660E+01

  QDA : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQDA
  QFA : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQFA

  KQDAM1 := -2.754317363207 !-0.27535473123509E+01
  KQFAM1 :=  2.759129754303 ! 0.27591003111614E+01
  KQDAM2 := -2.74803100438  !-0.27480718251248E+01
  KQFAM2 :=  2.7620476125   ! 0.27621020663312E+01

  QDAM1 : QDA, K1 = KQDAM1
  QFAM1 : QFA, K1 = KQFAM1
  QDAM2 : QDA, K1 = KQDAM2
  QFAM2 : QFA, K1 = KQFAM2

  KQDAS1 := -2.748447933552 !-0.27479594904701E+01
  KQFAS1 :=  2.755721311701 ! 0.27556435289068E+01
  KQDAS2 := -2.754511493351 !-0.27534461947460E+01
  KQFAS2 :=  2.757896206261 ! 0.27577774003461E+01

  QDAS1 : QDA, K1 = KQDAS1
  QFAS1 : QFA, K1 = KQFAS1
  QDAS2 : QDA, K1 = KQDAS2
  QFAS2 : QFA, K1 = KQFAS2

  KQFAH := 2.41134183863  ! 0.24113326137009E+01

  QDAH : QDA, K1 = -KQFAH !-0.24113326137009E+01
  QFAH : QFA, K1 =  KQFAH ! 0.24113326137009E+01

  KQFAV := 2.411355711866 ! 0.24110397544602E+01

  QDAV : QDA, K1 = -KQFAV !-0.24113128778319E+01
  QFAV : QFA, K1 =  KQFAV ! 0.24110397544602E+01

! match to spin rotator (solenoids ON)

  KQMTS1 :=  3.96678221747  ! 4.005861E+00    
  KQMTS2 := -2.950996573045 !-2.849187E+00    
  KQMTS3 :=  5.439131392443 ! 5.315159E+00    
  KQMTS4 := -3.479152403771 !-3.406870E+00    

  QMTS1 : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQMTS1
  QMTS2 : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQMTS2
  QMTS3 : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQMTS3
  QMTS4 : QUAD, TYPE="Q20L200", L=LQA/2, APER=rQ7, K1=KQMTS4

! ==============================================================================
! DRIFTs
! ------------------------------------------------------------------------------

  DMTS1  : DRIF, L=2.0
  DMTS2  : DRIF, L=1.5
  DMTS3  : DRIF, L=0.4
  DMTS4  : DRIF, L=0.4

! ==============================================================================
! COLLs
! ------------------------------------------------------------------------------

! adjustable spoilers, here set to 1.5! points of the horizontal dogleg

  SPHDOG : RCOLL, XSIZE = 980.E-6, YSIZE = 10.E-3

! fixed absorbers at 6.5 mm aperture to match the ones in the collimation
! section

  ABHDOG : ECOLL, XSIZE = 6.5E-3, YSIZE = 6.5E-3

! ==============================================================================
! MARKs
! ------------------------------------------------------------------------------

  BEGINPTURN : MARK
  ENDPTURN   : MARK

! ==============================================================================
! BEAMLINEs
! ------------------------------------------------------------------------------

! Standard cell

  TURNCELL : LINE = (                            DFODO, BAF, DFODO, &
                     2*QDA, BPMQ079,       YCOR, DFODO, BAF, DFODO, &
                     2*QFA, BPMQ079 ,XCOR, YCOR )

! dispersion matching cell

  TURNMATCH : LINE = (                                DFODO, BD1F, DFODO, &
                       2*QDAM1, BPMQ079 ,       YCOR, DFODO, BD1F, DFODO, &
                       2*QFAM1, BPMQ079 ,XCOR , YCOR, DFODO, BD2F, DFODO, &
                       2*QDAM2, BPMQ079 ,       YCOR, DFODO, BD2F, DFODO, &
                       2*QFAM2, BPMQ079 ,XCOR , YCOR )

! dispersion suppression cell

  TURNSUPP : LINE = (                                DFODO, BD3F, DFODO, &
                       2*QDAS1, BPMQ079,       YCOR, DFODO, BD3F, DFODO, &
                       2*QFAS1, BPMQ079, XCOR, YCOR, DFODO, BD4F, DFODO, &
                       2*QDAS2, BPMQ079,       YCOR, DFODO, BD4F, DFODO, &
                       2*QFAS2, BPMQ079, XCOR, YCOR                        )

! Reverse bend cells 

  TURNCELLR : LINE = (                            DFODO, BAR, DFODO, &
                      2*QDA, BPMQ079,       YCOR, DFODO, BAR, DFODO, &
                      2*QFA, BPMQ079, XCOR, YCOR )


  TURNMATCHR : LINE = (                               DFODO, BD1R, DFODO, &
                        2*QDAM1, BPMQ079,       YCOR, DFODO, BD1R, DFODO, &
                        2*QFAM1, BPMQ079, XCOR, YCOR, DFODO, BD2R, DFODO, &
                        2*QDAM2, BPMQ079,       YCOR, DFODO, BD2R, DFODO, &
                        2*QFAM2, BPMQ079, XCOR, YCOR )

  TURNSUPPR : LINE = (                                DFODO, BD3R, DFODO, &
                        2*QDAS1, BPMQ079,       YCOR, DFODO, BD3R, DFODO, &
                        2*QFAS1, BPMQ079, XCOR, YCOR, DFODO, BD4R, DFODO, &
                        2*QDAS2, BPMQ079,       YCOR, DFODO, BD4R, DFODO    )


! vertical dogleg

  VDOG : LINE = (                             DFODO, BV1, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV1, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV2, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV2, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV2, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV2, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV1, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV1, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV3, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV3, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV4, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV4, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV4, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV4, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR, DFODO, BV3, DFODO, &
                 2*QDAV, BPMQ079,       YCOR, DFODO, BV3, DFODO, &
                 2*QFAV, BPMQ079, XCOR, YCOR )

! horizontal dogleg

  HDOG : LINE = (                             DFODO, BH1, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH1, DFODO, &
                 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH2, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH2, DFODO, &
         SPHDOG, 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH2, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH2, DFODO, &
         ABHDOG, 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH1, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH1, DFODO, &
                 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH3, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH3, DFODO, &
                 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH4, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH4, DFODO, &
         SPHDOG, 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH4, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH4, DFODO, &
         ABHDOG, 2*QFAH, BPMQ079, XCOR, YCOR, DFODO, BH3, DFODO, &
                 2*QDAH, BPMQ079,       YCOR, DFODO, BH3, DFODO, &
                 2*QFAH, BPMQ079, XCOR, YCOR )
!
! Match into spin rotator
!
  SPINMATCH : LINE = (&
   2*QMTS1, BPMQ079, XCOR, YCOR, DMTS1, &
   2*QMTS2, BPMQ079, XCOR, YCOR, DMTS2, &
   2*QMTS3, BPMQ079, XCOR, YCOR, DMTS3, &
   2*QMTS4, BPMQ079, XCOR, YCOR, DMTS4    )

! Complete system:

  PTURN : LINE = ( BEGINPTURN, HDOG, VDOG, &
                   TURNMATCH,  18*TURNCELL, TURNSUPP, &
                   TURNMATCHR, 3*TURNCELLR, TURNSUPPR, SPINMATCH, ENDPTURN )

! ------------------------------------------------------------------------------

  RETURN

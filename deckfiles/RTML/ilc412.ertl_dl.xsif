!========1=========2=========3=========4=========5=========6=========7=========8
! 
! In this file we provide no-collimation-solution for the extraction line in 
! BC1
!
!===============================================================================
! 15-Apr-2015, M. Woodley (SLAC)
!    ILC2015a release
! 10-Apr-2015, M. Woodley
!    Change "QD10" to "QD10DL" ... "QD10" is used in Final Focus
! 10-Oct-2012, Sergei Seletskiy
!    Created
!===============================================================================
!  ASSIGN, PRINT="BC1EL_PRINT.txt"
!  ASSIGN, ECHO ="BC1EL_ECHO.txt"
!===============================================================================

! initial Twiss

  ERTLDBETX := 15.511026278431   !twiss beta x (m)
  ERTLDALFX := -0.738872746799   !twiss alpha x
  ERTLDDX   := 0.028787529352    !eta x (m)
  ERTLDDPX  := 0.772931455531E-3 !eta' x
  ERTLDBETY := 46.058719161121   !twiss beta y (m)
  ERTLDALFY := 0.645343530312    !twiss alpha y
  ERTLDDY   := 0.0               !eta y (m)
  ERTLDDPY  := 0.0               !eta' y (rad)

  TWSS_ERTLD : BETA0, &
    ENERGY=E0_RTML, &
    BETX=ERTLDBETX , &
    ALFX=ERTLDALFX , &
    DX=ERTLDDX , &
    DPX=ERTLDDPX , &
    BETY=ERTLDBETY , &
    ALFY=ERTLDALFY , &
    DY=ERTLDDY , &
    DPY=ERTLDDPY                

! initial SURVEY coordinates

  ERTLD_x     := 50.36195206644
  ERTLD_y     := 1.65
  ERTLD_z     := -2.660913313E2
  ERTLD_theta := 3.552033457091
  ERTLD_phi   := 0
  ERTLD_psi   := 0

!======== Beamline defenitions ========

! parameters
  Lb0=1
  ANGb0=0.094*Lb0/1
  Lq0=0.5
  Lq0fin=0.6

! MAGNETS

! bends:
  B_ECSEPT_A : SBEND, TYPE="SEPTUM BEND", L = 0.5, ANGLE = 3.4E-03,     &   
                      E1 = 1.7E-03, E2 = 0, FINT = 0.5, FINTX = 0
  B_ECSEPT_B : SBEND, TYPE="SEPTUM BEND", L = 0.5, ANGLE = 3.4E-03,     &   
                     E1 = 0, E2 = 1.7E-03, FINT = 0, FINTX = 0.5
  B_ECSEPT : LINE = (B_ECSEPT_A, B_ECSEPT_B)
!  B1 : SBEND, L = Lb0, ANGLE = ANGb0, E1 = ANGb0/2, E2 = ANGb0/2
  B_ECBEXT : SBEND, TYPE="EXTRACTION BEND", L = Lb0, ANGLE = ANGb0,       &   
                   E1 = ANGb0/2, E2 = ANGb0/2     

! quads:
  QDFIT10 : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0, APERTURE = 0.05,  &   
                 K1 =  1.372419236055   
  QDFIT20 : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0, APERTURE = 0.05,   &   
                 K1 =  2.184527459918   
  QD10DL : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0, APERTURE = 0.05,      &    
              K1 =  1.866787097819   
  QD20 : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0, APERTURE = 0.05,      &    
              K1 =  1.86679121266    
  Q10 : QUAD, TYPE="QUAD_DUMPLINE", L=Lq0, APERTURE = 0.05,         &    
             K1=-2.166174831594         
  Q20 : QUAD, TYPE="QUAD_DUMPLINE", L=Lq0, APERTURE = 0.05,         &     
             K1=2.285594774123
  QFIN10 : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0fin, APERTURE = 0.05, &   
                K1 =  -2.5
  QFIN20 : QUAD, TYPE="QUAD_DUMPLINE", L = Lq0fin, APERTURE = 0.05, &   
                K1 = -2.5

!sextupoles:
! SF1 : SEXTUPOLE, L = 0.3, APERTURE = 0.05, K2=0
! SF2 : SEXTUPOLE, L = 0.2, APERTURE = 0.05, K2=0
!  SF3 : SEXTUPOLE, L = 1.0, APERTURE = 0.05, K2=90
!  SF4 : SEXTUPOLE, L = 0.5, APERTURE = 0.05, K2=90

! drifts:
  D: DRIFT, L=0.5
!  DR30CM : DRIFT, L = 0.3
  DR20CM : DRIFT, L = 0.2
  DR10CM : DRIFT, L = 0.1
  DFIN10 : DRIFT, L = 0.2
  DFIN20 : DRIFT, L = 4.5

  ENDERTLD : MARK

! line:
  DISP_FIT0 : LINE= (QDFIT10, dr30cm, B_ECBEXT, dr10cm, dr30cm, dr10cm, &
                    QDFIT20, d, B_ECBEXT, d)

  EC_DL : LINE= (4*(B_ECSEPT,DR30CM),B_ECSEPT,DR20CM, DISP_FIT0,&
                   q10, d, q20, d,&
                   B_ECBEXT, dr10cm, DR20CM, dr10cm, QD10DL, qd20, &
                   dr10cm, DR20CM, dr10cm, B_ECBEXT, &
                   dr10cm, dr10cm, qfin10, dfin10, qfin20, dr10cm, &
                   dfin20, ENDERTLD)

!======================================



!======== Twiss parameters ========
  TWISS_0disp0 :  BETA0, BETX = 16.6578249148, ALFX = -1.20109678383, &
                        BETY = 32.259695957, ALFY =  1.64768052715, &
                        DX = 0.0340354346372, DPX = 0.00110260595165
!==================================

!
!
!
!======== Plotting results ========
!
!  SETPLOT, LWIDTH=3, ASCALE=1.0, LSCALE=1.0, RSCALE=1.0, &
!           XSIZE=21, YSIZE=15
!  OPTION, DOUBLE
!
!
!  use, (line_fin)
!
!  TWISS, beta0=TWISS_0disp, TAPE="BC1EL_TWISS.txt", SAVE
! 
!  plot, table=twiss, haxis=s,vaxis1=betx, vaxis2=bety,style=100, &
!        filename="BC1EL"
!  plot, table=twiss, haxis=s,vaxis1=dx,dpx,style = 100,filename="BC1EL"
!
!==================================
!
!  use, line_fin
!  Select, optics, full
!  OPTICS,BETA0=TWISS_0disp ,             &
!  FILE = "BC1EL_optics.out"  ,           &
!  COLUMNS=name, KEYWORD, S, L,           & 
!          BETX, ALFX, BETY, ALFY, DX, DPX
!
!use, line_fin
!match,   BETA0=TWISS_0disp   
!    rmatrix, range = #s/#e
!endmatch

RETURN       
!========1=========2=========3=========4=========5=========6=========7=========8
!
!   ELTL.xsif is a integration of a part of getaway & return 
! 
!------------------------------------------------------------------------------
!
!  ILC RTML e- <<getaway line>>
!  ! V.Kapin & N.Solyak: version 2011/10/10 
!   This file is based on "ilc.501.ecentral.mad8" May 2010 designed by A.Latina
! 
!------------------------------------------------------------------------------
!
! ILC RTML e- return line -- ERETURN.xsif
!
! Lattice file for the electron return line, which brings the beam from the
! bottom of the escalator (now excluded) to the start of the turnaround.
!
! This lattice requires the following files to run properly:
!
!    RTML_common.xsif
!
! plus one of
!
!    RTML_sigz_nominal.xsif
!    RTML_sizg300A.xsif
!    RTML_sigzLowN.xsif
!    RTML_sigz150A.xsif
!
! In addition, the kink in coordinates due to the earth's curvature, 
! EKINK, must be defined in the caller (since this is different in MAD-8
! and in beamlines which have a GKICK element).
!
! Auth:  PT, 13-dec-2006.
! Mod:  
!   15-Apr-2015, M. Woodley (SLAC)
!      ILC2015a release
!   27-mar-2015, M. Woodley (SLAC)
!    * lengthen system for DKS main linac plus some general rematching
!   19-nov-2007, PT:
!    * remove VALUE statement accidentally introduced earlier.
!   09-oct-2007, M. Woodley
!    * remove 11 RETURNCELLCs (was 158, now 147)
!    * tweak length of DERFODO drift (there are 355 of them in ERETURN)
!   23-feb-2012, AV
!        AV, 2/23/2012 
!    * modified length of DERFODO to match treaty point specs;
!    * also added a return cell ... good luck!
! 
!-----------------------------------------------------------------------------!
! VK, 2011/05/26:   "ilc503.ereturn.xsif" is                                   ! 
! a modification of "ilc403.ereturn.xsif"                                      !
!                                                                              ! 
!  a) the matching section "ERMATCH" is removed (by redifining as MARKER);     !
!  b) Redefine first cell of ERETURN: "ERET1ST"                                !
!  c) change beamline                                                          !
!     ! VK20110506 RETURNCELLM(YCETA1), is replaced by "ERET1ST":              !
!  d) add 12 RETURNCELLCs (was 147, now 159) to increase global Zmax=15,201m;  !
!------------------------------------------------------------------------------!
!

!2011/12/14 Initial parameters at entry to the "GETAWAY"-LINE at match-to-SS:
GETAW_MTSS_START: BETA0, BETX=1.65566, ALFX=+0.52551, &
                         BETY=8.82907, ALFY=-2.50611

! Initial parameters at entry to the return line:
  TWISS_ERETURN : BETA0, BETX = 4.7964, ALFX = -2.3888, &
                         BETY = 0.9485, ALFY =  0.5712

  BEAM_ERETURN  :  BEAM, ENERGY = 5, NPART = BunchCharge, &
                   SIGE = 1.5E-3, SIGT = 6.E-03, &
                   EX = 8E-6 * EMASS/5, EY = 20E-9 * EMASS/5

! ==============================================================================
! Quads
! ------------------------------------------------------------------------------

! quads for matching from the arc to the scs
! rematched values 2011/12/14:

  KQDASM1 = -3.93928921863
  KQFASM1 =  3.054046E+00 !not changed
  KQDASM2 = -3.612135E+00 !not changed
  KQFASM2 =  2.266598E+00 !not changed           

  QDASM1 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KQDASM1
  QFASM1 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KQFASM1
  QDASM2 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KQDASM2
  QFASM2 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KQFASM2

! 45/45 cell for the skew correction and diagnostic section

  KSCS45 := 0.17947529496151E+01 ! about 0.3 T pole-tip

  QFSCS1 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 =  KSCS45
  QDSCS1 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = -KSCS45

! Quads for the 180/90 insertions in the SCS

  KFSCS2 :=  2.881663918178 ! 0.28817425042996E+01 ! about 0.5 T pole-tip
  KDSCS2 := -2.407481363053 !-0.24075562630531E+01

  QFSCS2 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KFSCS2
  QDSCS2 : QUAD, TYPE="Q20L200", L = 0.2/2, APERTURE = 0.01, K1 = KDSCS2

! Quads for matching into and out of the DR stretch straight

  KQFSSM1 :=  0.440472329579 ! 4.386146E-01    
  KQDSSM1 := -0.928655408827 !-9.301393E-01    
  KQFSSM2 :=  1.210766498931 ! 1.210973E+00    
  KQDSSM2 := -1.191026614122 !-1.190980E+00    

  QFSSM1 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=KQFSSM1    
  QDSSM1 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=KQDSSM1    
  QFSSM2 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=KQFSSM2    
  QDSSM2 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=KQDSSM2    
    
  QFSSM3 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1= 7.386545E-02    
  QDSSM3 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=-5.953084E-01    
  QFSSM4 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1= 1.134937E+00    
  QDSSM4 : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=-1.019872E+00    

! quads for the DR stretch

  QFSTRETCH : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1= 0.103930720826 ! 1.035896E-01    
  QDSTRETCH : QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=-0.103930720826 !-1.035896E-01    

! skew quads for coupling correction

  SCS_SQ1 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0
  SCS_SQ2 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0
  SCS_SQ3 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0
  SCS_SQ4 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0

! Skew quads for dispersion correction (arc 2 only)

  SQGA1 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0
  SQGA2 : QUAD, TYPE="Q20L100", L=LQ3/2, APER=RQ3, TILT, K1=0

!--------------------------------------------------------

! Quadrupoles in the matching region from the escalator into the return line
! QF1ERM : QUAD, TYPE="Q20L200", L = 0.1, APER = 0.01, K1 =  1.039683E+00    
! QD1ERM : QUAD, TYPE="Q20L200", L = 0.1, APER = 0.01, K1 = -1.134834E+00    
! QF2ERM : QUAD, TYPE="Q20L200", L = 0.1, APER = 0.01, K1 =  5.810963E-01    
! QD2ERM : QUAD, TYPE="Q20L200", L = 0.1, APER = 0.01, K1 = -6.436235E-02    

! Quadrupoles in the matching region from the return line to the turnaround

  QF1RTM : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1= 0.360147523321 ! 5.539566E-01    
  QD1RTM : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=-0.71829804148  !-8.538169E-01    
  QF2RTM : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1= 1.496375613884 ! 1.355329E+00    
  QD2RTM : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=-3.597820847399 !-4.607947E+00    
  QF3RTM : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1= 1.064211655592 ! 2.4113

! Quads in the return line proper

  QFRETURN : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1= 0.10547504398 ! 1.057713E-01    
  QDRETURN : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=-0.10547504398 !-1.057713E-01    

! MATCH6: match to ereturn
QMERET2: QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1=-0.106366347283 !-0.107114560716  
QMERET4: QUAD, TYPE="Q20L200", L=0.2/2, APER=0.01, K1= 0.10341309192  !+0.102993590187

! <<Target_cell>> near positron target with special 39m drift:
QFRET_5cell : QFRETURN, K1=+0.10568268442;
QDRET_5cell : QDRETURN, K1=-0.33703611205;       
QFRET_6cell : QFRETURN, K1=+0.11686633299;
QDRET_6cell : QDRETURN, K1=-0.126049377236 !-0.12464177198;

! MATCH8 <<3Q>> match Target_cell to H_DLeg_TRG  L=50m :

  KQ_MTCH8A :=  0.193302916524 !+0.19227118694  ! 1st Quad-MaTCH-8 denoted as A
  KQ_MTCH8B := -0.232416407065 !-0.232897699543 ! 2nd Quad-MaTCH-8 denoted as B
  KQ_MTCH8C := -0.154678501727 !-0.154947916418 ! 3rd Quad-MaTCH-8 denoted as C

  Q_MTCH8A : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH8A
  Q_MTCH8B : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH8B
  Q_MTCH8C : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH8C

! H_DLeg_TRG - Horizontal DogLeg near positron TaRGet : 
                                                               
 QF_HDL_TRG : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1= 0.347336235347 !+0.347336232401 !Quad-Foc
 QD_HDL_TRG : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=-0.347312293797 !-0.347312291515 !Quad-Defoc

! MATCH9 <<3Q>> match H_DLeg_TRG to ERETURN FODO  L=50m:

  KQ_MTCH9A :=  0.119527250033 !-0.253799957382 ! 1st Quad-MaTCH-9 denoted as A
  KQ_MTCH9B :=  0.283940241806 !+0.275823198456 ! 2nd Quad-MaTCH-9 denoted as B
  KQ_MTCH9C := -0.381488881419 !-0.357332122355 ! 3rd Quad-MaTCH-9 denoted as C

  Q_MTCH9A : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH9A
  Q_MTCH9B : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH9B
  Q_MTCH9C : QUAD, TYPE="Q20L200", L=0.1, APER=0.01, K1=KQ_MTCH9C

! ==============================================================================
! Sector bends
! ------------------------------------------------------------------------------

! bends for chicane which is used to extract laser wire signal -- for now,
! use a 2 cm total offset in 4 0.9 meter bends
! (NOTE[MW]: FINT/FINTX not defined

  BGLW1 : SBEND, TYPE="D25L900V1", L=0.9, HGAP=GB30/2, ANGLE= Bsign*0.025, & 
                 E1= Bsign*0.0125, E2= Bsign*0.0125
  BGLW2 : SBEND, TYPE="D25L900V1", L=0.9, HGAP=GB30/2, ANGLE=-Bsign*0.025, & 
                 E1=-Bsign*0.0125, E2=-Bsign*0.0125
  BGLW3 : SBEND, TYPE="D25L900V1", L=0.9, HGAP=GB30/2, ANGLE=-Bsign*0.025, & 
                 E1=-Bsign*0.0125, E2=-Bsign*0.0125
  BGLW4 : SBEND, TYPE="D25L900V1", L=0.9, HGAP=GB30/2, ANGLE= Bsign*0.025, & 
                 E1= Bsign*0.0125, E2= Bsign*0.0125

! Bends for hor. dogleg DX=3.755m to avoid positron target: 
                                       !
  A1_HDL_TRG := -0.004172447*Bsign !Angle of Bend 
  A2_HDL_TRG := -A1_HDL_TRG        !Angle of the opposite bend

  B1_HDL_TRG : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE= A1_HDL_TRG, & 
                      E1= A1_HDL_TRG/2, E2= A1_HDL_TRG/2
  B2_HDL_TRG : SBEND, TYPE="D25L2300", L=2.3, HGAP=HGBA, ANGLE= A2_HDL_TRG, & 
                      E1= A2_HDL_TRG/2, E2= A2_HDL_TRG/2

! ==============================================================================
! Drifts
! ------------------------------------------------------------------------------


! arc-SCS matching drift

  DASM : DRIFT, L = 1.7

! skew correction section drift, and drift with space reserved for skew quads

  DSCS1  : DRIFT, L = 2.0
  DSCS1s : DRIFT, L = DSCS1[L] - DR30CM[L] - 2*SCS_SQ1[L]

! drifts for 180/90 insertion in SCS

  DSCS2  : DRIFT, L = 2.2497
  DSCS2s : DRIFT, L = DSCS2[L] - DR30CM[L] - 2*SCS_SQ1[L]
  DSCS3  : DRIFT, L = 2.0744

! drift for wire scanner space

  DWS : DRIFT, L = SCS_SQ1[L]

! drifts for matching regions into and out of the stretch

  D1SSM : DRIFT, L = 1.6
  D2SSM : DRIFT, L = 10.8 - 4*DR30CM[L] - BGLW1[L] - BGLW2[L] &
                                        - BGLW3[L] - BGLW4[L]
  D3SSM : DRIFT, L = 1.3
  D4SSM : DRIFT, L = 9.9

  D5SSM : DRIFT, L = 3
  D6SSM : DRIFT, L = 0.75745916551760E+01
  D7SSM : DRIFT, L = 0.43244043819188E+01
  D8SSM : DRIFT, L = 0.47635417662622E+01
  D9SSM : DRIFT, L = 4.3

! drift for the stretch itself

  DSTRETCH : DRIFT, L = 36.68774258681 !36.809

!---------------------------------------------------------------------

! drifts in the matching region from the escalator to the return line
!  D1ERM : DRIFT, L = 3; 
!  D2ERM : DRIFT, L = 4.764; 
!  D3ERM : DRIFT, L = 4.324
!  D4ERM : DRIFT, L = 7.575; 
!  D5ERM : DRIFT, L = 3

! drifts in the matching region from the return line to the turnaround

 ! D1RTM : DRIFT, L = 3
 ! D2RTM : DRIFT, L = 7.575
 ! D3RTM : DRIFT, L = 4.324
 ! D4RTM : DRIFT, L = 4.764
 ! D5RTM : DRIFT, L = 3

  D1RTM : DRIFT, L = 8.7
  D2RTM : DRIFT, L = 3.2
  D3RTM : DRIFT, L = 9.3
  D4RTM : DRIFT, L = 2.5
  D5RTM : DRIFT, L = 1.1

! drift in the long FODO cell (original length: 36.047)
 
  NDERFODO := 355 !2*(7+147+7+16)+1
  DDERFODO := -20.134605849839
  LDERFODO := 36.148625093011 !36.016344259004

! It used to be (NDERFODO*36.047+DDERFODO)/NDERFODO;

  DERFODO : DRIFT, L = LDERFODO

! MATCH6: match to ereturn: (Drift-Match-ERETurn-N):

  LDMERET := 35.278503 + 37.959719 + 40.361777

  LDMERET1 := 35.929713705191 !35.278503; 
  LDMERET3 := 38.058704852695 !37.959719;                           
  LDMERET5 := 39.611580442114 !40.361777;                           

  DMERET1: DRIFT, L=LDMERET1 
  DMERET3: DRIFT, L=LDMERET3                           
  DMERET5: DRIFT, L=LDMERET-(LDMERET1+LDMERET3)                           

! <<Target_cell>> near positron target with special 39m drift:

  D_P_target: DRIFT, L=39;      ! Drift-Positron target                                  
  D1_6cell: DRIFT, L=7.647809;  ! special Drift 1 of the 6th cell 
  D2_6cell: DRIFT, L=6.561909;  ! special Drift 2 of the 6th cell 

! MATCH8 <<3Q>> match Target_cell to H_DLeg_TRG  L=50m :

  L_MTCH8 := 14.919602576611 + 30.169554415749 + 4.610843008127

  L_MTCH8A := 14.919602576611 ! 1st Drift-MaTCH8 denoted as A
  L_MTCH8B := 30.169554415749 ! 2nd Drift-MaTCH8 denoted as B
  L_MTCH8C :=  4.610843008127 ! 3rd Drift-MaTCH8 denoted as C

  D_MTCH8A: DRIFT, L = L_MTCH8A
  D_MTCH8B: DRIFT, L = L_MTCH8B
  D_MTCH8C: DRIFT, L = L_MTCH8-(L_MTCH8A+L_MTCH8B)

! H_DLeg_TRG - Horizontal DogLeg near positron TaRGet:
                                                      
  D1125CM : DRIFT, L=11.25 !drift in FODO (11.25m)             

! MATCH9 <<3Q>> match H_DLeg_TRG to ERETURN FODO  L=50m :

  L_MTCH9 := 0.2 + 14.803108969693 + 7.561048201988 + 27.33584283001

  L_MTCH9A:= 16.599517341141 !14.803108969693 ! 1st Drift-MaTCH-9 denoted as A            
  L_MTCH9B:=  6.043660602102 ! 7.561048201988 ! 2nd Drift-MaTCH-9 denoted as B            
  L_MTCH9C:= 27.256822058448 !27.33584283001  ! 3rd Drift-MaTCH-9 denoted as C           

  D_MTCH9A: DRIFT, L=L_MTCH9A             
  D_MTCH9B: DRIFT, L=L_MTCH9B
  D_MTCH9C: DRIFT, L=L_MTCH9-(L_MTCH9A+L_MTCH9B)

! ==============================================================================
! COLLs
! ------------------------------------------------------------------------------

! Nominal settings for adjustable spoilers, collimating at 60sigy
! by 10sigx.  For 60sigy, and assuming "razor blade" spoilers, 
! the jitter-amplification factor is 0.76;
! this is probably not acceptable if the beam jitter from the DR
! is 1 sigma (goes to 1.25 sigmas downstream of the collimator, and
! emittance growth from wakefields is about 9! amplification factor is 0.069, which is fine (1 sigma jitter goes
! to 1.002 sigmas, emittance growth is 0.07
  SPOILY : RCOLL, XSIZE = 10.00e-3, YSIZE =  1.03e-3
  SPOILX : RCOLL, XSIZE =  3.43e-3, YSIZE = 10.00e-3

! Quad protection collimators have an aperture of 6.5 mm

  PCQUAD : ECOLL, XSIZE = 0.0065, YSIZE = 0.0065


! ==============================================================================
! Y correctors for following the curvature of the earth and dispersion matching
! ------------------------------------------------------------------------------

  CURVEON  := 1
  CURVEOFF := 0

  ECURVE = CURVEON !CURVEOFF

 !ECURVEANGLE := -1 * ECURVE * (2*QFRETURN[L] + DERFODO[L])/(6.370E6)
  ECURVEANGLE := -1 * ECURVE * (2*QFRETURN[L] + DERFODO[L])/R_earth

  YCRETURN : VKICK, TYPE="D20L50", KICK = ECURVEANGLE

  YCETA1 : VKICK, TYPE="D20L50", kick = -1.1118197486988499e-005 * ECURVE;
  YCETA2 : VKICK, TYPE="D20L50", kick = -4.8161756595588847e-006 * ECURVE;
  YCETA3 : VKICK, TYPE="D20L50", kick = -1.1120595408778373e-005 * ECURVE;
  YCETA4 : VKICK, TYPE="D20L50", kick = -4.8314296590361185e-006 * ECURVE;
  YCETA5 : VKICK, TYPE="D20L50", kick = -2.4642747065189261e-006 * ECURVE;
  YCETA6 : VKICK, TYPE="D20L50", kick = -1.0154921624029854e-005 * ECURVE;
  YCETA7 : VKICK, TYPE="D20L50", kick = -2.4517507800486406e-006 * ECURVE;
  YCETA8 : VKICK, TYPE="D20L50", kick = -1.0145877227551170e-005 * ECURVE;

  KRTMLQm : MULT,  K0L= -ECURVEANGLE, T0
  KRTMLQv : VKICK, KICK=-ECURVEANGLE

! ==============================================================================
! Markers
! ------------------------------------------------------------------------------

  BEGINEGETAWAY : MARKER
  ENDGETAWAY    : MARKER
  DUMPKICK      : MARKER ! for now just a marker
  PPS_STOPPER   : MARKER

  MRKGW1 : MARKER
  MRKGW2 : MARKER
  MRKGW3 : MARKER
  MRKGW4 : MARKER

  BEGINERETURN : MARKER
  ENDERETURN   : MARKER
  NOKINK       : MARKER
  ENDKINK      : MARKER

  MRK_QF_CNTR  :  MARKER ! Marker at CeNTeR of QFSTRETCH
! ==============================================================================
! BEAMLINES
! ------------------------------------------------------------------------------

! ==============================================================================
! MATCHING sections between beamlines
! ------------------------------------------------------------------------------

! MATCH6:  << 2Q >> match to ereturn -(2011/12/15) L=114m==!
                                                           !
begin_match6: marker; end_match6: marker                   !
                                                           !
Match6: LINE=(begin_match6, &                              !
          DMERET1,2*QMERET2,DMERET3,2*QMERET4,DMERET5, &   !
              end_match6);                                 !
                                                           !
!end MATCH6:  << 2Q >> match to ereturn ===================!

! <<Target_cell>> with special 39m drift ------------------!
! at positron target. L_cell=90m114m                       !
! It replaces two repetitive 5th & 6th cells of            ! 
! M * "RETURNCELLC"                                        !
                                                           !
targ_cell_S: marker; targ_cell_E: marker                   !
                                                           !
Target_cell: LINE=( targ_cell_S,                         & !
        2*QFRET_5cell, DERFODO, 2*QDRETURN, D_P_target,  & !
        2*QFRET_6cell, D1_6cell,2*QDRET_6cell, D2_6cell, & !
                    targ_cell_E)                           !
                                                           !
!end <<Target_cell>> --------------------------------------!

! MATCH8 <<4Q>> match Target_cell to H_DLeg_TRG  L=50m ====!
                                                           !
                                                           !
M8_START: MARKER; M8_END: MARKER;                          !
                                                           !
MATCH8 : LINE = ( M8_START,                            &   !          
            2*Q_MTCH8A, BPMQ079,       YCOR, D_MTCH8A, &   !
            2*Q_MTCH8B, BPMQ079, XCOR, YCOR, D_MTCH8B, &   !
            2*Q_MTCH8C, BPMQ079,       YCOR, D_MTCH8C, &   !
                                               M8_END)     !

!==========================================================!
!

! H_DLeg_TRG - Horizontal DogLeg near positron TaRGet---------------!
                                                                    !
Hdog_start: MARKER; Hdog_cntr: MARKER; Hdog_end: MARKER;            !
                                                                    !
HDL_1arc: LINE = (                                                & !
 2*QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
 2*QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
 2*QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B1_HDL_TRG, D1125CM, & !
   QF_HDL_TRG     )                                                 !
                                                                    !
HDL_2arc: LINE = (                                                & !
   QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B2_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B2_HDL_TRG, D1125CM, & !
 2*QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B2_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B2_HDL_TRG, D1125CM, & !
 2*QF_HDL_TRG, BPMQ079, XCOR, YCOR, D1125CM, B2_HDL_TRG, D1125CM, & !
 2*QD_HDL_TRG, BPMQ079,       YCOR, D1125CM, B2_HDL_TRG, D1125CM  & !
                  )                                                 !
                                                                    !
! periodical FODO Hdogleg structure 48 meters long                  !
H_DLeg_TRG: LINE = (Hdog_start,                                   & !
                    HDL_1arc, Hdog_cntr, HDL_2arc, Hdog_end)        !
                                                                    !
!-------------------------------------------------------------------!

! MATCH9 <<3Q>> match H_DLeg_TRG to ERETURN FODO  L=50m ---!
                                                           !
M9_START: MARKER; M9_END: MARKER;                          !
                                                           !
MATCH9 : LINE = ( M9_START,                            &   !          
            2*Q_MTCH9A, BPMQ079,       YCOR, D_MTCH9A, &   !
            2*Q_MTCH9B, BPMQ079, XCOR, YCOR, D_MTCH9B, &   !
            2*Q_MTCH9C, BPMQ079,       YCOR, D_MTCH9C, &   !
                                               M9_END)     !

!----------------------------------------------------------!


GETAWAY : LINE = (BEGINEGETAWAY,           &  
! 
      2*QDASM1, BPMQ079,       YCOR, DASM, &
      2*QFASM1, BPMQ079, XCOR, YCOR, DASM, &
      2*QDASM2, BPMQ079,       YCOR, DASM, &
      2*QFASM2, BPMQ079, XCOR, YCOR, DASM, &
MRKGW1, &
!
! Skew correction and emittance measurement section -- a 4-wire section only,
! but with optics slots to accommodate the remaining 2 wires needed for 4D
! emittance purposes in an upgrade
!
                                    2*SCS_SQ1, DR30CM, &
      2*QDSCS1, BPMQ079,       YCOR, DSCS1,            &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DSCS1,            &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1s,           &
                                    2*SCS_SQ2, DR30CM, &
      2*QDSCS1, BPMQ079,       YCOR, DSCS2,            &
      2*QFSCS2, BPMQ079, XCOR, YCOR, DSCS3,            &
      2*QDSCS2, BPMQ079,       YCOR, DSCS3,            &
      2*QFSCS2, BPMQ079, XCOR, YCOR, DSCS2s,           &
                                    2*SCS_SQ3, DR30CM, &
      2*QDSCS1, BPMQ079,       YCOR, DSCS1,            &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DSCS1,            &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1s,           &
                                    2*SCS_SQ4, DR30CM, &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS,     DWS, DSCS1s, &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DSCS1,            &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS,     DWS, DSCS2s, &
      2*QFSCS2, BPMQ079, XCOR, YCOR, DSCS3,            &
      2*QDSCS2, BPMQ079,       YCOR, DSCS3,            &
      2*QFSCS2, BPMQ079, XCOR, YCOR, DSCS2,            &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS, WS, DWS, DSCS1s, &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS, WS, DWS, DSCS1s, &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS, WS, DWS, DSCS1s, &
      2*QFSCS1, BPMQ079, XCOR, YCOR, DSCS1,            &
      2*QDSCS1, BPMQ079,       YCOR, DR30CM,           &
                                 DWS, WS, DWS, DSCS1s, &
MRKGW2, &
!
! match to the DR stretch including chicane for capturing light
! from the laser wires
!
      2*QFSSM1, BPMQ079, XCOR, YCOR, D1SSM,  &
      2*QDSSM1, BPMQ079,       YCOR, DR30CM, &
        BGLW1, DR30CM, BGLW2, DR30CM,        &
        BGLW3, DR30CM, BGLW4, D2SSM,         &
      2*QFSSM2, BPMQ079, XCOR, YCOR, D3SSM,  &
      2*QDSSM2, BPMQ079,       YCOR, D4SSM,  &
MRKGW3, &
! 
! DR stretch including post-DR collimation
!
                QFSTRETCH,                                &
              MRK_QF_CNTR,                                &
                QFSTRETCH,                                &
                           BPMQ079, XCOR, YCOR,           &
                                        SPOILX, DSTRETCH, &
      PCQUAD, 2*QDSTRETCH, BPMQ079,       YCOR,           &
                                        SPOILY, DSTRETCH, &
      PCQUAD, 2*QFSTRETCH, BPMQ079, XCOR, YCOR, DSTRETCH, &
      PCQUAD, 2*QDSTRETCH, BPMQ079,       YCOR, DSTRETCH, &
      PCQUAD, 2*QFSTRETCH, BPMQ079, XCOR, YCOR,           &
                                        SPOILX, DSTRETCH, &
      PCQUAD, 2*QDSTRETCH, BPMQ079,       YCOR,           &
                                        SPOILY, DSTRETCH, &
      PCQUAD, 2*QFSTRETCH, BPMQ079, XCOR, YCOR, DSTRETCH, &
      PCQUAD, 2*QDSTRETCH, BPMQ079,       YCOR, DSTRETCH, &
      PCQUAD,                                             &
              2*QFSTRETCH, BPMQ079, XCOR, YCOR, DSTRETCH, &
              2*QDSTRETCH, BPMQ079,       YCOR,           &
      DUMPKICK,                                 DSTRETCH, & 
   7 * (      2*QFSTRETCH, BPMQ079, XCOR, YCOR, DSTRETCH, &
              2*QDSTRETCH, BPMQ079,       YCOR, DSTRETCH),&
              2*QFSTRETCH, BPMQ079, XCOR, YCOR,           &
      3 * PPS_STOPPER, & 
!VK20110506: -----------------------------------------------
!                                               DSTRETCH, &
!              2*QDSTRETCH, BPMQ079,       YCOR,           &
      MATCH6, & 
!VK20110506 END --------------------------------------------
!
! match out of DR stretch into escalator
!
!MRKGW4, &
!      D5SSM, &
!      2*QFSSM3, BPMQ079, XCOR, YCOR, D6SSM, &
!      2*QDSSM3, BPMQ079,       YCOR, D7SSM, &
!      2*QFSSM4, BPMQ079, XCOR, YCOR, D8SSM, &
!      2*QDSSM4, BPMQ079,       YCOR, D9SSM, &
!
! and that's it
! 
      ENDGETAWAY                                         )

!------------------------------------------------------------------------------

  EKINK : LINE=(KRTMLQm,KRTMLQv)


! There are 3 kinds of return cell:  the standard curved cell, the standard
! straight cell, and the curved cell with dispersion matching correctors.  
! We handle all this with formal parameters:

  RETURNCELL(Y1,Y2,KINK) : LINE = (                &          
     2*QFRETURN, BPMQ079, XCOR, Y1, KINK, DERFODO, &
     2*QDRETURN, BPMQ079,       Y2, KINK, DERFODO    )

! now define the families of cells

  RETURNCELLC     : LINE = (RETURNCELL(YCRETURN,YCRETURN,EKINK))
  RETURNCELLS     : LINE = (RETURNCELL(YCOR,YCOR,NOKINK))
  RETURNCELLM(YC) : LINE = (RETURNCELL(YC,YCRETURN,EKINK))

! The entry matcher

!VK 20110506 remove ERMATCH by redifining as MARKER
!  ERMATCH : LINE = (D1ERM, 2*QF1ERM, BPMQ079, XCOR, YCOR, &
!                    D2ERM, 2*QD1ERM, BPMQ079,       YCOR, &
!                    D3ERM, 2*QF2ERM, BPMQ079, XCOR, YCOR, &
!                    D4ERM, 2*QD2ERM, BPMQ079,       YCOR, &
!                    D5ERM)
!VK 20110506 remove ERMATCH by redifining as MARKER

ERMATCH: MARKER; 

! VK 20110506 Redefine first cell of ERETURN

ERET1ST:  LINE = (                &          
! 2*QFRETURN, BPMQ079, XCOR, YCETA1, EKINK, DERFODO, &
! 2*QDRETURN, BPMQ079,       YCETA2, EKINK, DERFODO    )
  2*QDRETURN, BPMQ079,       YCOR, NOKINK, DERFODO    )

!VK 20110506 END ===========================================


! The exit matcher

  RTMATCH : LINE = (D1RTM, 2*QF1RTM, BPMQ079, XCOR, YCOR, &
                    D2RTM, 2*QD1RTM, BPMQ079,       YCOR, &
                    D3RTM, 2*QF2RTM, BPMQ079, XCOR, YCOR, &
                    D4RTM, 2*QD2RTM, BPMQ079,       YCOR, &
                    D5RTM, 2*QF3RTM)


M_5cell_start: MARKER; M_11_cell_end: MARKER; 

! The beamline:

  ERETURN : LINE = (BEGINERETURN, ERMATCH, &
!
! cells 1, 3, 5, and 7 have correctors to perform dispersion match
!
!VK20110506 RETURNCELLM(YCETA1), is replaced by "ERET1ST":  
     ERET1ST, &
!                          RETURNCELLC, RETURNCELLM(YCETA2), RETURNCELLC,  &
!     RETURNCELLM(YCETA1), RETURNCELLC, RETURNCELLM(YCETA2),               &
                          RETURNCELLS, RETURNCELLS, RETURNCELLS,           &
      RETURNCELLS, RETURNCELLS, RETURNCELLS,                               &
!
! then we have 147 standard cells
! VK20110526: add 12 cells for new design to match global Zmax~15196m
!             147+12=159
                       4*RETURNCELLS, M_5cell_start, Target_cell,         &
                          MATCH8, H_DLeg_TRG, MATCH9,                     &
!                       7*RETURNCELLC, M_11_cell_end,                      &
     RETURNCELLM(YCETA1), RETURNCELLC, RETURNCELLM(YCETA2), RETURNCELLC,  &
     RETURNCELLM(YCETA3), RETURNCELLC, RETURNCELLM(YCETA4), M_11_cell_end, &
                     148 * RETURNCELLC,                                   &

! then cells with correctors to perform the dispersion match on exit
!
     RETURNCELLM(YCETA5), RETURNCELLC, RETURNCELLM(YCETA6), RETURNCELLC,   &
     RETURNCELLM(YCETA7), RETURNCELLC, RETURNCELLM(YCETA8), ENDKINK,       &
!
! then 16 straight cells
!
     16 * RETURNCELLS,                                                    &
! 
! then a straight cell lacking its final drift
!
     2*QFRETURN, BPMQ079, XCOR, YCOR, DERFODO, 2*QDRETURN, BPMQ079, YCOR, &
!
! match into the turnaround and end
!
     RTMATCH, ENDERETURN )
!
!
ELTL : LINE = (GETAWAY, ERETURN)

! ------------------------------------------------------------------------------
! temporary beamlines for matching
! ------------------------------------------------------------------------------

SKFODO1 : LINE=(&
  QDSCS1, BPMQ079,       YCOR, DSCS1, QFSCS1, &
  QFSCS1, BPMQ079, XCOR, YCOR, DSCS1, QDSCS1  )

SKFODO2 : LINE=(&
  QDSCS1, BPMQ079,       YCOR, DSCS2, QFSCS2, &
  QFSCS2, BPMQ079, XCOR, YCOR, DSCS3, QDSCS2, &
  QDSCS2, BPMQ079,       YCOR, DSCS3, QFSCS2, &
  QFSCS2, BPMQ079, XCOR, YCOR, DSCS2s, 2*SCS_SQ3, DR30CM, QDSCS1 )

SFODO : LINE=(&
  QFSTRETCH,BPMQ079,XCOR,YCOR,DSTRETCH,PCQUAD,QDSTRETCH, &
  QDSTRETCH,BPMQ079,     YCOR,DSTRETCH,PCQUAD,QFSTRETCH)

DLFODO : LINE=(&
  QF_HDL_TRG,BPMQ079,XCOR,YCOR,D1125CM,B1_HDL_TRG,D1125CM,QD_HDL_TRG, &
  QD_HDL_TRG,BPMQ079      YCOR,D1125CM,B1_HDL_TRG,D1125CM,QF_HDL_TRG)

! ------------------------------------------------------------------------------

RETURN

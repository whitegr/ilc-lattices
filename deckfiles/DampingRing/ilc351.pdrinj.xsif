! ==============================================================================
! area=pdrinj
! ------------------------------------------------------------------------------
! 15-Apr-2015, M. Woodley (SLAC)
!    ILC2015a release
! 25-AUG-2014, M. Woodley
!   * based on:
!     - DTC04 Damping Ring lattice
!     - "Targeted Specifications for the DR-RTML and DR-Source Treaty Points",
!       M. Palmer, 15-SEP-2011
!     - ILC TDR, Volume 3, Part II, section 6.9 (pp 117-118)
!     - "AmericaRegion DrawingSet for KILC Draft April-23-2012.pdf"
!     - "Injection, Extraction and Abort Lines for the ILC Damping Rings",
!       I. Reichel, 04-MAY-2007
! ------------------------------------------------------------------------------

! ==============================================================================
! match conditions (exit of 2nd PDR injection septum)
! ------------------------------------------------------------------------------

! beam

  TPDRINJ_BEAM : BEAM, &
    PARTICLE=POSITRON, & !particle type
    ENERGY=5.0, &        !beam energy (GeV)
    NPART=2.E10, &       !number of particles per bunch
    EXN=12.0E-3, &       !normalized horizontal emittance (m)
    EYN=12.0E-3, &       !normalized vertical emittance (m)
    SIGT=16.0E-3, &      !bunch length (m)
    SIGE=0.29E-2         !energy spread (1)

! Twiss

  TPDRINJ_TWISS : BETA0, &
    BETX=70.022513875576, &   !twiss beta x (m)
    ALFX=-0.017843264215, &   !twiss alpha x
    DX=0.083087722825, &      !twiss eta x (m)
    DPX=0.306876500864E-2, & !twiss etaprime x
    BETY=6.330781530313, &    !twiss beta y (m)
    ALFY=-0.205808784245, &   !twiss alpha y
    DY=0, &                   !twiss eta y (m)
    DPY=0                     !twiss etaprime y

! SURVEY coordinates

  TPDRINJ_X     := 100.0+(-0.084002142525)
  TPDRINJ_Y     := 0.35
  TPDRINJ_Z     := -78.70737
  TPDRINJ_Theta := 0+(-0.171220307588E-5)
  TPDRINJ_Phi   := 0
  TPDRINJ_Psi   := 0

  VALUE, TPDRINJ_X,TPDRINJ_Y,TPDRINJ_Z,TPDRINJ_Theta,TPDRINJ_Phi,TPDRINJ_Psi

! ==============================================================================
! initial conditions (treaty point TPS2DR)
! ------------------------------------------------------------------------------

! Twiss

  TPS2DR_TWISS : BETA0, &
    BETX=70.022513875581, &   !twiss beta x (m)
    ALFX=-0.017843264215, &   !twiss alpha x
    DX=0.083087722825, &      !twiss eta x (m)
    DPX=-0.306876500865E-2, & !twiss etaprime x
    BETY=6.330781530312, &    !twiss beta y (m)
    ALFY=-0.205808784245, &   !twiss alpha y
    DY=0, &                   !twiss eta y (m)
    DPY=0                     !twiss etaprime y

! SURVEY coordinates

  TPS2DR_X     := 94.41    !9.441018878E+01
  TPS2DR_Y     := 0.35     !3.500206181E-01
  TPS2DR_Z     := -112.103 !-1.121030511E+02
  TPS2DR_Theta := 0.24     !2.400010955E-01
  TPS2DR_Phi   := 0
  TPS2DR_Psi   := 0

  BRHO_PDRINJ := CB*TPDRINJ_BEAM[ENERGY]

! ==============================================================================
! BENDs
! ------------------------------------------------------------------------------

! 2nd injection septum (30 mm)

  Lsep2 := 1.0                           !efective length (m)
  Gsep2 := 0.02                          !gap height (m)
  Bsep2 := 10.8                          !pole-tip field (kG)
  Asep2 := Bsign*Bsep2*Lsep2/BRHO_PDRINJ !bend angle (rad)

  SEPINJ2a : SBEN, TYPE="APS_30mm", L=Lsep2/2, ANGLE=Asep2/2, HGAP=Gsep2/2, &
                   E1=Asep2/2, FINT=0.5, FINTX=0
  SEPINJ2b : SBEN, TYPE="APS_30mm", L=Lsep2/2, ANGLE=Asep2/2, HGAP=Gsep2/2, &
                   E2=Asep2/2, FINT=0, FINTX=0.5

! 1st injection septum (2 mm)

  Lsep1 := 1.0                           !efective length (m)
  Gsep1 := 0.02                          !gap height (m)
  Bsep1 := 7.3                           !pole-tip field (kG)
  Asep1 := Bsign*Bsep1*Lsep1/BRHO_PDRINJ !bend angle (rad)

  SEPINJ1a : SBEN, TYPE="APS_2mm", L=Lsep1/2, ANGLE=Asep1/2, HGAP=Gsep1/2, &
                   E1=Asep1/2, FINT=0.5, FINTX=0
  SEPINJ1b : SBEN, TYPE="APS_2mm", L=Lsep1/2, ANGLE=Asep1/2, HGAP=Gsep1/2, &
                   E2=Asep1/2, FINT=0, FINTX=0.5

! dispersion suppressor bend

  LB1INJ := 2.0                  !effective length (m)
  GB1INJ := 0.06                 !gap height (m) ... guess
  AB1INJ := Bsign*0.131477937919 !bend angle (rad)

  B1INJa : SBEN, TYPE="D60L2000", L=LB1INJ/2, ANGLE=AB1INJ/2, HGAP=GB1INJ/2, &
                 E1=AB1INJ/2, FINT=0.5, FINTX=0
  B1INJb : SBEN, TYPE="D60L2000", L=LB1INJ/2, ANGLE=AB1INJ/2, HGAP=GB1INJ/2, &
                 E2=AB1INJ/2, FINT=0, FINTX=0.5

! ==============================================================================
! QUAD
! ------------------------------------------------------------------------------

  QPDRINJ1 : QUAD, L=0.15, K1= 1.293097165255
  QPDRINJ2 : QUAD, L=0.15, K1=-1.251587350087
  QPDRINJ3 : QUAD, L=0.15, K1= 0.893708383251
  QPDRINJ4 : QUAD, L=0.15, K1=-1.048673863382
  QPDRINJ5 : QUAD, L=0.15, K1= 0.948115600223

! ==============================================================================
! DRIFTs
! ------------------------------------------------------------------------------

  LDEL0a := 15.117028125736 !B1/septum separation

  DEL0a : DRIF, L=LDEL0a

  LmdwZ   := LDEL0a-5*(2*0.15) !available drift length (assuming 5 quadrupoles)
  LPDRINJ := LmdwZ/8

  LDPDRINJ0 := 3.303533709223
  LDPDRINJ1 := 1.209138477849
  LDPDRINJ2 := 1.668231077051
  LDPDRINJ3 := 1.611090317525
  LDPDRINJ4 := 2.078368467945
  LDPDRINJ5 := LmdwZ-(LDPDRINJ0+LDPDRINJ1+LDPDRINJ2+LDPDRINJ3+LDPDRINJ4)

  dLinj0 := 0
  dLinj1 := 0
  dLinj2 := 0
  dLinj3 := 0
  dLinj4 := 0

  DPDRINJ0 : DRIF, L=LDPDRINJ0       +dLinj0 !2*LPDRINJ
  DPDRINJ1 : DRIF, L=LDPDRINJ1-dLinj0+dLinj1 !  LPDRINJ
  DPDRINJ2 : DRIF, L=LDPDRINJ2-dLinj1+dLinj2 !  LPDRINJ
  DPDRINJ3 : DRIF, L=LDPDRINJ3-dLinj2+dLinj3 !  LPDRINJ
  DPDRINJ4 : DRIF, L=LDPDRINJ4-dLinj3+dLinj4 !  LPDRINJ
  DPDRINJ5 : DRIF, L=LDPDRINJ5-dLinj4        !2*LPDRINJ
  DSEP     : DRIF, L=0.5

 !VALUE, DPDRINJ0[L]+DPDRINJ1[L]+DPDRINJ2[L]+DPDRINJ3[L]+DPDRINJ4[L]+&
 !       DPDRINJ5[L],LmdwZ

! ==============================================================================
! MARKERs
! ------------------------------------------------------------------------------

  BEGPDRINJ : MARK
  ENDPDRINJ : MARK

! ==============================================================================
! BEAMLINEs
! ------------------------------------------------------------------------------

  PDRINJ0 : LINE=(BEGPDRINJ,&
    B1INJa,B1INJb,DEL0a,&
    SEPINJ2a,SEPINJ2b,DSEP,&
    SEPINJ1a,SEPINJ1b,&
    ENDPDRINJ)

  PDRINJ : LINE=(BEGPDRINJ,&
    B1INJa,B1INJb,DPDRINJ0,&
    QPDRINJ1,QPDRINJ1,DPDRINJ1,&
    QPDRINJ2,QPDRINJ2,DPDRINJ2,&
    QPDRINJ3,QPDRINJ3,DPDRINJ3,&
    QPDRINJ4,QPDRINJ4,DPDRINJ4,&
    QPDRINJ5,QPDRINJ5,DPDRINJ5,&
    SEPINJ2a,SEPINJ2b,DSEP,&
    SEPINJ1a,SEPINJ1b,&
    ENDPDRINJ)

  PDRINJr : LINE=(ENDPDRINJ,&
    SEPINJ1a,SEPINJ1b,DSEP,&
    SEPINJ2a,SEPINJ2b,DPDRINJ5,&
    QPDRINJ5,QPDRINJ5,DPDRINJ4,&
    QPDRINJ4,QPDRINJ4,DPDRINJ3,&
    QPDRINJ3,QPDRINJ3,DPDRINJ2,&
    QPDRINJ2,QPDRINJ2,DPDRINJ1,&
    QPDRINJ1,QPDRINJ1,DPDRINJ0,&
    B1INJa,B1INJb,&
    BEGPDRINJ)

  PDRINJrh : LINE=(ENDPDRINJ,&
    SEPINJ1a,SEPINJ1b,DSEP,&
    SEPINJ2a,SEPINJ2b,DPDRINJ5,&
    QPDRINJ5,QPDRINJ5,DPDRINJ4,&
    QPDRINJ4,QPDRINJ4,DPDRINJ3,&
    QPDRINJ3)

! ==============================================================================

  RETURN

  TITLE "ILC e- Source (ILC2015a)"

  OPTION, -INTER, DOUBLE, VERIFY, -ECHO
  ASSIGN, PRINT="eSource.print"
  ASSIGN, ECHO="eSource.echo"

! ==============================================================================
! History
! ------------------------------------------------------------------------------
! 15-Apr-2015, M. Woodley (SLAC)
!    ILC2015a release
! 02-DEC-2014, M. Woodley
!    Rematch everything
! 08-OCT-2014, M. Woodley
!    Download ES2012a.zip from EDMS:
!      ID          = D00000000976695,B,1,1
!      Name        = ILC Electron Source Lattice 2012a
!      Work Status = Released
!      Last Update = 05.05.2014 12:01:37
! ------------------------------------------------------------------------------

! ==============================================================================
! Common parameters, definitions, and configurations
! ------------------------------------------------------------------------------

  Cb    := 1.E10/CLIGHT !kG-m/GeV
  Bsign := -1           !clockwise (e-)

  E0 := 0.07567
  Ef := 5.0

! BL new values for PLTR from Wanming (Feb. 2012): 17.2.2012

  EGUN_x     :=94.41-85.491446495771    !94.41-8.549159407E+01
  EGUN_y     :=0
  EGUN_z     :=112.103+7.020081093176E2 !112.103+7.020080137E+02
  EGUN_theta :=PI+0.007
  EGUN_phi   :=0
  EGUN_psi   :=0

 !VALUE, EGUN_x,EGUN_y,EGUN_z,EGUN_theta,EGUN_phi,EGUN_psi

  BXm := 1
  AXm := 0
  BYm := 1
  AYm := 1

  TWSSm : BETA0, BETX=BXm, ALFX=AXm, BETY=BYm, ALFY=AYm

! ==============================================================================
! Beamline definitions
! ------------------------------------------------------------------------------

  HC  : HKIC
  VC  : VKIC
  BPM : MONI

  CALL "../../deckfiles/eSource/ilc101.echic.xsif"
  CALL "../../deckfiles/eSource/ilc111.ebstr.xsif"
  CALL "../../deckfiles/eSource/ilc121.eltr.xsif"
  CALL "../../deckfiles/DampingRing/ilc311.edrinj.xsif"

  ESOURCE : LINE=(ECHIC,EBSTR,ELTR,EDRINJ)

! ==============================================================================
! COMMANDs
! ------------------------------------------------------------------------------

  SETPLOT, XSIZE=25.4, YSIZE=20.32
  SETPLOT, LWIDTH=5, LSCALE=1.5, SSCALE=1.5, RSCALE=1.5
  OPTION, ECHO

! matching

 !CALL "eSource_match.mad8"

! generate output files and plots

 !COMMENT !survey
    BEAM, ENERGY=BEAM_ECHIC[ENERGY]
    USE, ESOURCE
    PRINT, FULL
    SURVEY, X0=EGUN_x, Y0=EGUN_y, Z0=EGUN_z, &
      THETA0=EGUN_theta, PHI0=EGUN_phi, PSI0=EGUN_psi, &
      TAPE="eSource_survey.tape"
 !ENDCOMMENT

 !COMMENT !Twiss
    BEAM, ENERGY=BEAM_ECHIC[ENERGY]
    USE, ESOURCE
    PRINT, FULL
    TWISS, COUPLE, BETA0=TWISS_ECHIC, SAVE, TAPE="eSource_twiss.tape"

    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=ENERGY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="eSource"

    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=ECHIC, TITLE="ECHIC"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=ECHIC, TITLE="ECHIC"

    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=ENERGY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=EBSTR, TITLE="EBSTR"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=EBSTR, TITLE="EBSTR"

    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=ELTR, TITLE="ELTR"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=ELTR, TITLE="ELTR"

    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=EDRINJ, TITLE="EDRINJ"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=EDRINJ, TITLE="EDRINJ"
 !ENDCOMMENT

! ------------------------------------------------------------------------------

  STOP


! ILC e- Source (ILC2015a)

  OPTION, -ECHO

  Bmax := 50
  Dmin := -0.6

! ==============================================================================
! SUBROUTINEs
! ------------------------------------------------------------------------------
! global geometry matching
! ------------------------------------------------------------------------------

  MGEO1 : SUBROUTINE !match CFS geometry
    BEAM, ENERGY=E0
    VALUE, LDmat45,AB1INJ/Bsign,LDEL0a
    USE, (ECHIC,EBSTR,ELTR,EDRINJ0)
    MATCH, BETA0=TWISS_ECHIC, SURVEY, &
      XS=EGUN_x, YS=EGUN_y, ZS=EGUN_z, &
      THETAS=EGUN_theta, PHIS=EGUN_phi, PSIS=EGUN_psi
      VARY, LDmat45, STEP=1.E-6
      VARY, AB1INJ,  STEP=1.E-6
      VARY, LDEL0a,  STEP=1.E-6
      WEIGHT, XS=1, ZS=1, THETAS=1
      CONSTR, #E, XS=TEDRINJ_X, ZS=TEDRINJ_Z, THETAS=TEDRINJ_Theta
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, LDmat45,AB1INJ/Bsign,LDEL0a
    PRINT, FULL
    SURVEY, &
      X0=EGUN_x, Y0=EGUN_y, Z0=EGUN_z, &
      THETA0=EGUN_theta, PHI0=EGUN_phi, PSI0=EGUN_psi
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MGEO2 : SUBROUTINE !locate TES2DR treaty point
    BEAM, ENERGY=E0
    VALUE, DFLTRB2a[L]
    USE, (ECHIC,EBSTR,ELTR)
    MATCH, BETA0=TWISS_ECHIC, SURVEY, &
      XS=EGUN_x, YS=EGUN_y, ZS=EGUN_z, &
      THETAS=EGUN_theta, PHIS=EGUN_phi, PSIS=EGUN_psi
      VARY, DFLTRB2a[L], STEP=1.E-6
      WEIGHT, XS=1, ZS=1
      CONSTR, TES2DR, XS=TES2DR_X, ZS=TES2DR_Z
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, DFLTRB2a[L]
    PRINT, FULL
    SURVEY, &
      X0=EGUN_x, Y0=EGUN_y, Z0=EGUN_z, &
      THETA0=EGUN_theta, PHI0=EGUN_phi, PSI0=EGUN_psi
  ENDSUBROUTINE

! ------------------------------------------------------------------------------
! ECHIC matching
! ------------------------------------------------------------------------------

  MECHIC1 : SUBROUTINE !match 2-dogleg chicane optics
    BEAM, ENERGY=E0
    VALUE, KQFdog,KQDdog,dLdog
    USE, ECHIC1
    MATCH, BETX=1, BETY=1
      VARY, KQFdog, STEP=1.E-6, LOWER=0
      VARY, KQDdog, STEP=1.E-6, UPPER=0
     !VARY, dLdog,  STEP=1.E-6
      CONSTR, Mdog1, DY=0
      CONSTR, Mdog2, DY=0, DPY=0
      RMATRIX, B1dog1/B2dog1, RM(2,1)=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, KQFdog,KQDdog,dLdog
    USE, ECHIC2
    PRINT, FULL
    TWISS, LINE=ECHIC1, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MECHIC2 : SUBROUTINE !match into 2-dogleg chicane
    BEAM, ENERGY=E0
    VALUE, Qmatinj1[K1],Qmatinj2[K1],Qmatinj3[K1],Qmatinj4[K1],Qmatinj5[K1]
    USE, ECHIC
    MATCH, BETA0=TWISS_ECHIC
      VARY, Qmatinj1[K1], STEP=1.E-6, UPPER=0
      VARY, Qmatinj2[K1], STEP=1.E-6, LOWER=0
      VARY, Qmatinj3[K1], STEP=1.E-6, UPPER=0
      VARY, Qmatinj4[K1], STEP=1.E-6, LOWER=0
      VARY, Qmatinj5[K1], STEP=1.E-6, UPPER=0
      CONSTR, Qmatinj3[1], BETY<15
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, QFdogA0[1], LINE=ECHIC1
     !CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Qmatinj1[K1],Qmatinj2[K1],Qmatinj3[K1],Qmatinj4[K1],Qmatinj5[K1]
    PRINT, FULL
    TWISS, BETA0=TWISS_ECHIC, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MECHIC3 : SUBROUTINE !match 45 deg emittance measurement FODO
    BEAM, ENERGY=E0
    VALUE, Qemitdch[K1],Qemitfch[K1]
    USE, FODOE
    CELL
      VARY, Qemitdch[K1], STEP=1.E-6, UPPER=0
      VARY, Qemitfch[K1], STEP=1.E-6, LOWER=0
      CONSTR, #E, MUX=45/360, MUY=45/360
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Qemitdch[K1],Qemitfch[K1]
    PRINT, FULL
    TWISS, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MECHIC4 : SUBROUTINE !match from 2-dogleg chicane to emittance measurement
    BEAM, ENERGY=E0
    VALUE, Qmat11ch[K1],Qmat12ch[K1],Qmat13ch[K1],Qmat14ch[K1]
    VALUE, LDmat10,LDmat11,LDmat12,LDmat13,LDmat14
    USE, ECHIC
    MATCH, BETA0=TWISS_ECHIC
      VARY, Qmat11ch[K1], STEP=1.E-6, UPPER=0
      VARY, Qmat12ch[K1], STEP=1.E-6, LOWER=0
      VARY, Qmat13ch[K1], STEP=1.E-6, UPPER=0
      VARY, Qmat14ch[K1], STEP=1.E-6, LOWER=0
     !VARY, LDmat10,      STEP=1.E-6, LOWER=0.1
     !VARY, LDmat11,      STEP=1.E-6, LOWER=0.1
     !VARY, LDmat12,      STEP=1.E-6, LOWER=0.1
     !VARY, LDmat13,      STEP=1.E-6, LOWER=0.1
     !CONSTR, Qmat11ch[1], BETX<6, BETY<6
     !CONSTR, Qmat12ch[1], BETX<6, BETY<6
     !CONSTR, Qmat13ch[1], BETX<6, BETY<6
     !CONSTR, Qmat14ch[1], BETX<6, BETY<6
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, Mmat1ch_end, LINE=FODOE
     !CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Qmat11ch[K1],Qmat12ch[K1],Qmat13ch[K1],Qmat14ch[K1]
    VALUE, LDmat10,LDmat11,LDmat12,LDmat13,LDmat14
    PRINT, FULL
    TWISS, BETA0=TWISS_ECHIC, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------
! EBSTR matching
! ------------------------------------------------------------------------------

  MEBSTR0 : SUBROUTINE !set beam energy profile
    VALUE, grad
    BEAM, ENERGY=E0
    USE, EBSTR
    MATCH, BETX=1, BETY=1
      VARY, grad, STEP=1.E-6
      WEIGHT, ENERGY=1
      CONSTR, #E, ENERGY=Ef
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, grad
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MEBSTR1 : SUBROUTINE !match EBSTR section 1 FODO cell (coasting)
    BEAM, ENERGY=E0
    VALUE, KQSEC1
    USE, EBSTR1
    CELL
      VARY, KQSEC1, STEP=1.E-6, LOWER=0
      CONSTR, #E, MUX=72/360, MUY=72/360
     !LMDIF, TOL=1.E-20
     !MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, KQSEC1
    PRINT, FULL
    TWISS, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      TITLE="EBSTR Section 1 (coasting)"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MEBSTR2 : SUBROUTINE !match into EBSTR section 1
    BEAM, ENERGY=E0
    VALUE, Qmat21ch[K1],Qmat22ch[K1],Qmat23ch[K1],Qmat24ch[K1]
    VALUE, LDmat20,LDmat21,LDmat22,LDmat23,LDmat24
    SET, MLquade4[K1],  KQSEC1
    SET, MLquade5[K1], -KQSEC1
    SET, MLquade6[K1],  KQSEC1
    SET, MLquade7[K1], -KQSEC1
    SET, MLquade8[K1],  KQSEC1
    USE, (ECHIC,MAT2CH,SEC1)
    MATCH, BETA0=TWISS_ECHIC
      VARY, Qmat21ch[K1], STEP=1.E-6, UPPER=0
      VARY, Qmat22ch[K1], STEP=1.E-6, LOWER=0
      VARY, Qmat23ch[K1], STEP=1.E-6, UPPER=0
      VARY, Qmat24ch[K1], STEP=1.E-6, LOWER=0
     !VARY, LDmat20,      STEP=1.E-6, LOWER=1.0
     !VARY, LDmat21,      STEP=1.E-6, LOWER=1.0
     !VARY, LDmat22,      STEP=1.E-6, LOWER=1.0
     !VARY, LDmat23,      STEP=1.E-6, LOWER=1.0
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, #E, LINE=EBSTR1
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Qmat21ch[K1],Qmat22ch[K1],Qmat23ch[K1],Qmat24ch[K1]
    VALUE, LDmat20,LDmat21,LDmat22,LDmat23,LDmat24
    BEAM, ENERGY=E0
    PRINT, FULL
    TWISS, BETA0=TWISS_ECHIC, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=Mmat1ch_end/#E
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MEBSTR3 : SUBROUTINE !match EBSTR section 2 FODO cell (coasting)
    BEAM, ENERGY=E0
    VALUE, KQSEC2
    USE, EBSTR2
    CELL
      VARY, KQSEC2, STEP=1.E-6, LOWER=0
      CONSTR, #E, MUX=72/360, MUY=72/360
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, KQSEC2
    PRINT, FULL
    TWISS, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      TITLE="EBSTR Section 2 (coasting)"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MEBSTR4 : SUBROUTINE !match into EBSTR section 2
    BEAM, ENERGY=E0
    USE, EBSTR2
    SAVEBETA, TWSSm, MQ11
    TWISS
    VALUE, MLquade4[K1],MLquade5[K1],MLquade6[K1],MLquade7[K1],MLquade8[K1]
    VALUE, MLquade9[K1],MLquade10[K1],MLquade11[K1],MLquade12[K1]
    VALUE, MLquade13[K1],MLquade14[K1],MLquade15[K1],MLquade16[K1]
   !SET, MLquade16[K1], KQSEC2
    USE, (ECHIC,MAT2CH,SEC1,SEC2)
    MATCH, BETA0=TWISS_ECHIC
      VARY, MLquade4[K1],  STEP=1.E-6, LOWER=0
      VARY, MLquade5[K1],  STEP=1.E-6, UPPER=0
      VARY, MLquade6[K1],  STEP=1.E-6, LOWER=0
      VARY, MLquade7[K1],  STEP=1.E-6, UPPER=0
      VARY, MLquade8[K1],  STEP=1.E-6, LOWER=0
      VARY, MLquade9[K1],  STEP=1.E-6, UPPER=0
      VARY, MLquade10[K1], STEP=1.E-6, LOWER=0
      VARY, MLquade11[K1], STEP=1.E-6, UPPER=0
      VARY, MLquade12[K1], STEP=1.E-6, LOWER=0
      VARY, MLquade13[K1], STEP=1.E-6, UPPER=0
      VARY, MLquade14[K1], STEP=1.E-6, LOWER=0
      VARY, MLquade15[K1], STEP=1.E-6, UPPER=0
      VARY, MLquade16[K1], STEP=1.E-6, LOWER=0
      CONSTR, MQ5,  BETY<55 !TWSSm[BETY]
      CONSTR, MQ6,  BETX<60 !TWSSm[BETY]
      CONSTR, MQ7,  BETY<70 !TWSSm[BETY]
      CONSTR, MQ8,  BETX<80 !TWSSm[BETY]
      CONSTR, MQ9,  BETY<TWSSm[BETY]
      CONSTR, MQ10, BETX<TWSSm[BETY]
      CONSTR, MQ11, BETY<TWSSm[BETY] !, BETX=TWSSm[BETX]
      CONSTR, MQ12, BETX=TWSSm[BETY] !, BETY=TWSSm[BETX]
      CONSTR, MQ13, BETY=TWSSm[BETY], BETX=TWSSm[BETX]
      CONSTR, MQ14, BETX=TWSSm[BETY], BETY=TWSSm[BETX]
      CONSTR, MQ15, BETY=TWSSm[BETY], BETX=TWSSm[BETX]
      CONSTR, MQ16, BETX=TWSSm[BETY], BETY=TWSSm[BETX]
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, #E, LINE=EBSTR2
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
     !LMDIF, TOL=1.E-20
     !MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, MLquade4[K1],MLquade5[K1],MLquade6[K1],MLquade7[K1],MLquade8[K1]
    VALUE, MLquade9[K1],MLquade10[K1],MLquade11[K1],MLquade12[K1]
    VALUE, MLquade13[K1],MLquade14[K1],MLquade15[K1],MLquade16[K1]
    BEAM, ENERGY=E0
    PRINT, FULL
    TWISS, BETA0=TWISS_ECHIC, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      TITLE="ECHIC + EBSTR Sections 1 & 2"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------
! ELTR matching
! ------------------------------------------------------------------------------

  MELTR1 : SUBROUTINE !ELTR chicane dispersion and R56
    VALUE, Q1QDB1[K1],Q2QDB1[K1],Q1QDB2[K1],Q2QDB2[K1]
    BEAM, ENERGY=Ef
    USE, (DLPTHD1,PTHDCOL,DLPTHD1r)
    MATCH, BETX=1, BETY=1
      VARY, Q1QDB1[K1], STEP=1.E-6, LOWER=0
      VARY, Q2QDB1[K1], STEP=1.E-6, UPPER=0
      VARY, Q1QDB2[K1], STEP=1.E-6
      VARY, Q2QDB2[K1], STEP=1.E-6
      CONSTR, D4PTHD[1], DPX=0
      RMATRIX, #S/#E, RM(5,6)=0.75
      CONSTR, #E, DX=0, DPX=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Q1QDB1[K1],Q2QDB1[K1],Q1QDB2[K1],Q2QDB2[K1]
   !PRINT, FULL
    TWISS, BETX=1, BETY=1, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR chicane"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR2 : SUBROUTINE !match EBSTR to ELTR Chicane
    BEAM, ENERGY=E0
    USE, (ECHIC,EBSTR)
    SAVEBETA, TWSSm, MEND
    TWISS, COUPLE, BETA0=TWISS_ECHIC
    BEAM, ENERGY=TWSSm[ENERGY]
    USE, (MAT2TR,end_ebstr,ELTR)
    VALUE, QM1eb[K1],QM2eb[K1],QM3eb[K1],QM1PTHD[K1],QM2PTHD[K1]
    VALUE, DM1eb[L],DM2eb[L],DM3eb[L],DM4eb[L]
    MATCH, BETA0=TWSSm
      VARY, QM1eb[K1],   STEP=1.E-6, UPPER=0
      VARY, QM2eb[K1],   STEP=1.E-6, LOWER=0
      VARY, QM3eb[K1],   STEP=1.E-6, UPPER=0
      VARY, QM1PTHD[K1], STEP=1.E-6, LOWER=0
      VARY, QM2PTHD[K1], STEP=1.E-6, UPPER=0
     !VARY, DM1eb[L],    STEP=1.E-6, LOWER=1.0
     !VARY, DM2eb[L],    STEP=1.E-6 !, LOWER=2.0
     !VARY, DM3eb[L],    STEP=1.E-6 !, LOWER=2.0
     !CONSTR, QM2EB[1], BETX=70
      CONSTR, QM3EB[1], BETY=65
      CONSTR, MPTHDCOL, BETX=6, ALFX=0, BETY=6, ALFY=0
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QM1eb[K1],QM2eb[K1],QM3eb[K1],QM1PTHD[K1],QM2PTHD[K1]
    VALUE, DM1eb[L],DM2eb[L],DM3eb[L],DM4eb[L]
    PRINT, FULL
    SURVEY
    TWISS, BETA0=TWSSm, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/MPTHDCOL, TITLE="EBSTR to ELTR Match"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR3 : SUBROUTINE !ELTR arc #1 cell dispersion
    VALUE, QTRIP1[K1],QTRIP2[K1]
    BEAM, ENERGY=Ef
    USE, ARC0LTR
    MATCH, BETX=1, BETY=1
      VARY, QTRIP1[K1], STEP=1.E-6, UPPER=0
      VARY, QTRIP2[K1], STEP=1.E-6, LOWER=0
      CONSTR, #E, DX=0, DPX=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QTRIP1[K1],QTRIP2[K1]
    PRINT, FULL
    TWISS, BETX=1, BETY=1, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR arc #1"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR4 : SUBROUTINE !match ELTR Chicane to arc #1
    BEAM, ENERGY=Ef
    USE, TEMP1
    VALUE, QM1PTHD2[K1],QM2PTHD2[K1],QM1ARC0BLTR[K1],QM2ARC0BLTR[K1]
    MATCH, BETX=6, BETY=6
      VARY, QM1PTHD2[K1],    STEP=1.E-6 !, UPPER=0
      VARY, QM2PTHD2[K1],    STEP=1.E-6 !, LOWER=0
      VARY, QM1ARC0BLTR[K1], STEP=1.E-6 !, UPPER=0
      VARY, QM2ARC0BLTR[K1], STEP=1.E-6 !, LOWER=0
      CONSTR, #E, BETX=10, ALFX=0, BETY=10, ALFY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QM1PTHD2[K1],QM2PTHD2[K1],QM1ARC0BLTR[K1],QM2ARC0BLTR[K1]
    USE, TEMP1
    PRINT, FULL
    SURVEY
    TWISS, BETX=6, BETY=6, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR5 : SUBROUTINE !match ELTR arc #1 to compressor RF section
    BEAM, ENERGY=Ef
    USE, TEMP2
    VALUE, QRF1LTR[K1],QRF2LTR[K1],QRF3LTR[K1],QRF4LTR[K1]
    MATCH, BETX=10, BETY=10
      VARY, QRF1LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QRF2LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QRF3LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QRF4LTR[K1], STEP=1.E-6, UPPER=0
      CONSTR, MRF2LTR, BETX=10, ALFX=0, BETY=10, ALFY=0
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QRF1LTR[K1],QRF2LTR[K1],QRF3LTR[K1],QRF4LTR[K1]
    USE, (TEMP2,SOLLTR)
    PRINT, FULL
    SURVEY
    TWISS, BETX=10, BETY=10, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/MRF2LTR, TITLE="ELTR arc #1"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      TITLE="ELTR arc #1"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR6 : SUBROUTINE !match compressor RF section to spin rotator
    BEAM, ENERGY=Ef
    VALUE, QSOL1LTR[K1],QSOL2LTR[K1],QSOL3LTR[K1],QSOL4LTR[K1],QSOL5LTR[K1]
    USE, (TEMP3,SOLLTR)
    MATCH, BETX=10, BETY=10
      VARY, QSOL1LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QSOL2LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QSOL3LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QSOL4LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QSOL5LTR[K1], STEP=1.E-6, UPPER=0
      CONSTR, QSOL2LTR[1], BETX<35
      CONSTR, MSOL2LTR, BETX=10, ALFX=0, BETY=10, ALFY=0
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QSOL1LTR[K1],QSOL2LTR[K1],QSOL3LTR[K1],QSOL4LTR[K1],QSOL5LTR[K1]
   !USE, (TEMP3,SOLLTR,MBLTRD,6*FODOLTRD)
   !PRINT, FULL
    SURVEY
    TWISS, COUPLE, BETX=10, BETY=10, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/MSOL2LTR
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR7 : SUBROUTINE !ELTR FODO #1
    VALUE, Q1FODOLTRD[K1],Q2FODOLTRD[K1]
    BEAM, ENERGY=Ef
    USE, FODOLTRD
    CELL
      VARY, Q1FODOLTRD[K1], STEP=1.E-6, LOWER=0
      VARY, Q2FODOLTRD[K1], STEP=1.E-6, UPPER=0
      CONSTR, #E, MUX=60/360, MUY=60/360
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, Q1FODOLTRD[K1],Q2FODOLTRD[K1]
    USE, (6*FODOLTRD)
    PRINT, FULL
    TWISS, LINE=FODOLTRD, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR FODO #1"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR8 : SUBROUTINE !match spin rotator into FODO #1
    BEAM, ENERGY=Ef
    VALUE, QMB1LTRD[K1],QMB2LTRD[K1],QMB3LTRD[K1],QMB4LTRD[K1],QMB5LTRD[K1]
    USE, (TEMP4,MBLTRD)
    MATCH, BETX=10, BETY=10
      VARY, QMB1LTRD[K1], STEP=1.E-6, UPPER=0
      VARY, QMB2LTRD[K1], STEP=1.E-6, LOWER=0
      VARY, QMB3LTRD[K1], STEP=1.E-6, UPPER=0
      VARY, QMB4LTRD[K1], STEP=1.E-6, LOWER=0
      VARY, QMB5LTRD[K1], STEP=1.E-6, UPPER=0
      CONSTR, QMB2LTRD[1], BETX<30
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, #E, LINE=FODOLTRD
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QMB1LTRD[K1],QMB2LTRD[K1],QMB3LTRD[K1],QMB4LTRD[K1],QMB5LTRD[K1]
    USE, (TEMP4,MBLTRD,6*FODOLTRD)
    PRINT, FULL
    TWISS, COUPLE, BETX=10, BETY=10, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR9 : SUBROUTINE !match FODO #1 through arc #2
    BEAM, ENERGY=Ef
    VALUE, QME1LTRD[K1],QME2LTRD[K1],QME3LTRD[K1],QARC21LTR[K1],QARC22LTR[K1]
    USE, (MELTRD,ARC2LTR)
    MATCH, LINE=FODOLTRD
      VARY, QME1LTRD[K1],  STEP=1.E-6, LOWER=0
      VARY, QME2LTRD[K1],  STEP=1.E-6, UPPER=0
      VARY, QME3LTRD[K1],  STEP=1.E-6, LOWER=0
      VARY, QARC21LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QARC22LTR[K1], STEP=1.E-6, LOWER=0
      CONSTR, QME3LTRD[1], BETX<30
      CONSTR, QARC21LTR[1], BETY<30
      CONSTR, BBL21b[2], ALFX=0, ALFY=0, DPX=0
      LMDIF, TOL=1.E-20, CALLS=10000
      MIGRAD, TOL=1.E-20, CALLS=10000
    ENDMATCH
    VALUE, QME1LTRD[K1],QME2LTRD[K1],QME3LTRD[K1],QARC21LTR[K1],QARC22LTR[K1]
    USE, (6*FODOLTRD,MELTRD,ARC2LTR)
    PRINT, FULL
    TWISS, LINE=FODOLTRD, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR arc #2"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR arc #2"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR10 : SUBROUTINE !120 degree ELTR vertical dogleg FODO cell
    VALUE, QD01LTR[K1],QD02LTR[K1]
    BEAM, ENERGY=Ef
    USE, FODOVD
    CELL
      VARY, QD01LTR[K1], STEP=1.E-6, UPPER=0
      CONSTR, #E, MUY=120/360
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QD01LTR[K1],QD02LTR[K1]
    PRINT, FULL
    TWISS, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR vertical dogleg FODO"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR11 : SUBROUTINE !ELTR vertical dogleg dispersion
    VALUE, ABVDOG
    VALUE, dLVdog,DVD1[L],DVD2[L]
    BEAM, ENERGY=Ef
    USE, VDOGLEG
    MATCH, BETX=1, BETY=1, SURVEY, YS=0
      VARY, ABVDOG, STEP=1.E-6
      VARY, dLVdog, STEP=1.E-6
      WEIGHT, YS=1
      CONSTR, #E, YS=1.65, DY=0, DPY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, ABVDOG
    VALUE, dLVdog,DVD1[L],DVD2[L]
    MATCH, BETX=BXm, ALFX=AXm, BETY=BYm, ALFY=AYm
      VARY, BXm, STEP=1.E-6, LOWER=0
      VARY, AXm, STEP=1.E-6
      VARY, BYm, STEP=1.E-6, LOWER=0
      VARY, AYm, STEP=1.E-6
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, MVDL, LINE=FODOVD
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, BXm,AXm,BYm,AYm
    PRINT, FULL
    SURVEY, Y0=0
    TWISS, BETA0=TWSSm, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR vertical dogleg"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR vertical dogleg"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR12 : SUBROUTINE !match arc #2 to vertical dogleg
    BEAM, ENERGY=Ef
    VALUE, QMAT31LTR[K1],QMAT32LTR[K1],QMAT33LTR[K1],QMAT34LTR[K1],QMAT35LTR[K1]
    USE, (MELTRD,ARC2LTR,MAT3LTR,VDOGLEG)
    MATCH, LINE=FODOLTRD
      VARY, QMAT31LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QMAT32LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QMAT33LTR[K1], STEP=1.E-6, LOWER=0
      VARY, QMAT34LTR[K1], STEP=1.E-6, UPPER=0
      VARY, QMAT35LTR[K1], STEP=1.E-6, LOWER=0
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, MVDL, LINE=FODOVD
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20, CALLS=10000
      MIGRAD, TOL=1.E-20, CALLS=10000
    ENDMATCH
    VALUE, QMAT31LTR[K1],QMAT32LTR[K1],QMAT33LTR[K1],QMAT34LTR[K1],QMAT35LTR[K1]
    USE, (6*FODOLTRD,MELTRD,ARC2LTR,MAT3LTR,VDOGLEG)
   !PRINT, FULL
    SURVEY
    TWISS, LINE=FODOLTRD, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=BBL21b[2]/DVD1[2]
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR13 : SUBROUTINE !ELTR FODO #2
    VALUE, QDLTRB2[K1],QFLTRB2[K1]
    BEAM, ENERGY=Ef
    USE, LTRB2FODO
    CELL
      VARY, QDLTRB2[K1], STEP=1.E-6, UPPER=0
      VARY, QFLTRB2[K1], STEP=1.E-6, LOWER=0
      CONSTR, #E, MUX=120/360, MUY=120/360
     !LMDIF, TOL=1.E-20
     !MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QDLTRB2[K1],QFLTRB2[K1]
    USE, (11*LTRB2FODO)
   !PRINT, FULL
    TWISS, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=#S/#E, TITLE="ELTR FODO #2"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR14 : SUBROUTINE !match into FODO #2
    BEAM, ENERGY=Ef
    USE, VDOGLEG
    MATCH, BETX=BXm, ALFX=AXm, BETY=BYm, ALFY=AYm
      VARY, BXm, STEP=1.E-6, LOWER=0
      VARY, AXm, STEP=1.E-6
      VARY, BYm, STEP=1.E-6, LOWER=0
      VARY, AYm, STEP=1.E-6
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, MVDL, LINE=FODOVD
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, BXm,AXm,BYm,AYm
    VALUE, QVD2F1[K1],QVD2F2[K1],QVD2F3[K1],QVD2F4[K1]
    USE, (VDOGLEG,VD2FODO)
    MATCH, BETA0=TWSSm
      VARY, QVD2F1[K1], STEP=1.E-6, UPPER=0
      VARY, QVD2F2[K1], STEP=1.E-6, LOWER=0
      VARY, QVD2F3[K1], STEP=1.E-6, UPPER=0
      VARY, QVD2F4[K1], STEP=1.E-6, LOWER=0
      WEIGHT, DX=0, DPX=0, DY=0, DPY=0
      CONSTR, #E, LINE=LTRB2FODO
      LMDIF, TOL=1.E-20, CALLS=10000
      MIGRAD, TOL=1.E-20, CALLS=10000
    ENDMATCH
    VALUE, QVD2F1[K1],QVD2F2[K1],QVD2F3[K1],QVD2F4[K1]
    USE, (VDOGLEG,VD2FODO,2*LTRB2FODO)
    PRINT, FULL
    SURVEY
    TWISS, BETA0=TWSSm, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource", &
      RANGE=DVD1[1]/DFLTRB2[4]
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.F., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR15 : SUBROUTINE !match EINJ
    BEAM, ENERGY=Ef
    VALUE, QEDRINJ1[K1],QEDRINJ2[K1],QEDRINJ3[K1],QEDRINJ4[K1],QEDRINJ5[K1]
    VALUE, DEDRINJ0[L],DEDRINJ1[L],DEDRINJ2[L],DEDRINJ3[L],DEDRINJ4[L],&
           DEDRINJ5[L]
    VALUE, dLinj0,dLinj1,dLinj2,dLinj3,dLinj4
    USE, EDRINJrh !1st half of EDRINJ in reverse
    MATCH, &
        BETX= TEDRINJ_TWISS[BETX], &
        ALFX=-TEDRINJ_TWISS[ALFX], &
        DX  = TEDRINJ_TWISS[DX],   &
        DPX =-TEDRINJ_TWISS[DPX],  &
        BETY= TEDRINJ_TWISS[BETY], &
        ALFY=-TEDRINJ_TWISS[ALFY]
      VARY, QEDRINJ5[K1], STEP=1.E-6
      VARY, QEDRINJ4[K1], STEP=1.E-6
      VARY, QEDRINJ3[K1], STEP=1.E-6
      VARY, dLinj4,       STEP=1.E-6
      VARY, dLinj3,       STEP=1.E-6
      VARY, dLinj2,       STEP=1.E-6
      CONSTR, QEDRINJ5[1], DX=Dmin
      CONSTR, #E, ALFX=0, DPX=0, ALFY=0, DX=Dmin
      CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    USE, EDRINJr !EDRINJ in reverse
    MATCH, &
        BETX= TEDRINJ_TWISS[BETX], &
        ALFX=-TEDRINJ_TWISS[ALFX], &
        DX  = TEDRINJ_TWISS[DX],   &
        DPX =-TEDRINJ_TWISS[DPX],  &
        BETY= TEDRINJ_TWISS[BETY], &
        ALFY=-TEDRINJ_TWISS[ALFY]
      VARY, QEDRINJ2[K1], STEP=1.E-6
      VARY, QEDRINJ1[K1], STEP=1.E-6
      VARY, dLinj1,       STEP=1.E-6
      VARY, dLinj0,       STEP=1.E-6
      CONSTR, QEDRINJ1[1], BETX<70, DX=Dmin
      CONSTR, #E, DX=0, DPX=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QEDRINJ1[K1],QEDRINJ2[K1],QEDRINJ3[K1],QEDRINJ4[K1],QEDRINJ5[K1]
    VALUE, DEDRINJ0[L],DEDRINJ1[L],DEDRINJ2[L],DEDRINJ3[L],DEDRINJ4[L],&
           DEDRINJ5[L]
    VALUE, dLinj0,dLinj1,dLinj2,dLinj3,dLinj4
    PRINT, FULL
    SURVEY
    TWISS, SAVE, &
        BETX= TEDRINJ_TWISS[BETX], &
        ALFX=-TEDRINJ_TWISS[ALFX], &
        DX  = TEDRINJ_TWISS[DX],   &
        DPX =-TEDRINJ_TWISS[DPX],  &
        BETY= TEDRINJ_TWISS[BETY], &
        ALFY=-TEDRINJ_TWISS[ALFY]
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ------------------------------------------------------------------------------

  MELTR16 : SUBROUTINE !match ELTR to EDR
    BEAM, ENERGY=Ef
    VALUE, QMAT41LTR[K1],QMAT42LTR[K1],QMAT43LTR[K1],QMAT44LTR[K1],&
           QMAT45LTR[K1]
    VALUE, DMAT40LTR[L],DMAT41LTR[L],DMAT42LTR[L],DMAT43LTR[L],DMAT44LTR[L],&
           DMAT45LTR[L]
    USE, (MAT4LTR,EDRINJ)
    MATCH, LINE=LTRB2FODO
     !VARY, QMAT41LTR[K1], STEP=1.E-6, UPPER=0, LOWER=-2.0
      VARY, QMAT42LTR[K1], STEP=1.E-6, LOWER=0, UPPER=+2.0
      VARY, QMAT43LTR[K1], STEP=1.E-6, UPPER=0, LOWER=-2.0
      VARY, QMAT44LTR[K1], STEP=1.E-6, LOWER=0, UPPER=+2.0
      VARY, QMAT45LTR[K1], STEP=1.E-6, UPPER=0, LOWER=-2.0
     !VARY, dLmdw0,        STEP=1.E-6, LOWER=-0.2, UPPER=0.2
     !VARY, dLmdw1,        STEP=1.E-6, LOWER=-0.2, UPPER=0.2
     !VARY, dLmdw2,        STEP=1.E-6, LOWER=-0.2, UPPER=0.2
     !VARY, dLmdw3,        STEP=1.E-6, LOWER=-0.2, UPPER=0.2
     !VARY, dLmdw4,        STEP=1.E-6, LOWER=-0.2, UPPER=0.2
     !CONSTR, QMAT41LTR[1], BETX<Bmax, BETY<Bmax
     !CONSTR, QMAT42LTR[1], BETX<Bmax, BETY<Bmax
     !CONSTR, QMAT43LTR[1], BETX<Bmax, BETY<Bmax
     !CONSTR, QMAT44LTR[1], BETX<Bmax, BETY<Bmax
     !CONSTR, QMAT45LTR[1], BETX<Bmax, BETY<Bmax
      CONSTR, #E, &
        BETX=TEDRINJ_TWISS[BETX], &
        ALFX=TEDRINJ_TWISS[ALFX], &
        BETY=TEDRINJ_TWISS[BETY], &
        ALFY=TEDRINJ_TWISS[ALFY]
     !CONSTR, #E, X=0, PX=0, Y=0, PY=0
      LMDIF, TOL=1.E-20
      MIGRAD, TOL=1.E-20
    ENDMATCH
    VALUE, QMAT41LTR[K1],QMAT42LTR[K1],QMAT43LTR[K1],QMAT44LTR[K1],&
           QMAT45LTR[K1]
    VALUE, DMAT40LTR[L],DMAT41LTR[L],DMAT42LTR[L],DMAT43LTR[L],DMAT44LTR[L],&
           DMAT45LTR[L]
    PRINT, FULL
    SURVEY
    TWISS, LINE=LTRB2FODO, SAVE
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=BETX,BETY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
    PLOT, TABLE=TWISS, HAXIS=S, VAXIS=DX,DY, &
      STYLE=100, SPLINE=.T., FILE="eSource"
  ENDSUBROUTINE

! ==============================================================================
! COMMANDs
! ------------------------------------------------------------------------------

  OPTION, ECHO

 !MGEO1
 !MGEO2
 !MECHIC1
 !MECHIC2
 !MECHIC3
 !MECHIC4
 !MEBSTR0
 !MEBSTR1
 !MEBSTR2
 !MEBSTR3
 !MEBSTR4
 !MELTR1
 !MELTR2
 !MELTR3
 !MELTR4
 !MELTR5
 !MELTR6
 !MELTR7
 !MELTR8
 !MELTR9
 !MELTR10
 !MELTR11
 !MELTR12
 !MELTR13
 !MELTR14
 !MELTR15
 !MELTR16

! ------------------------------------------------------------------------------

  STOP

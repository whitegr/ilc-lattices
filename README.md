********************************************************************************
ILC2016x (31-Dec-2016)
********************************************************************************

================================================================================
This is the RC2016X branch of the repository for ILC baseline lattice
description files (i.e. MAD decks). This is release ILC2016x, which incorporates
Change Requests CR-002, CR-004, CR-006, CR-008, and CR-011.
================================================================================

Master MAD command files for optics generation are in the comfiles/ folder.
Each MAD file uses CALL statements to load common parameter definition files,
configuration files, and element/beamline definition files, depending on which
system is being defined.  Paths to the various files are constructed assuming
that a comfiles/ subfolder is the working directory when MAD is run.

There are two e- Source system MAD files (in comfiles/eSource):

 eSource.mad8       : e- gun, chicane, booster linac, and transport lines
 eSource_match.mad8 : MAD matching subroutines

There are two e+ Source system MAD files (in comfiles/pSource):

 pSource.mad8       : e+ production target, capture, pre-acceleration, booster
                      linac, energy compressor, path length adjuster, and
                      transport lines
 pSource_match.mad8 : MAD matching subroutines

There are two Damping Ring system MAD files (in comfiles/DampingRing):

 eDR.mad8 : e- Damping Ring (with injection and extraction systems)

 pDR.mad8 : e+ Damping Ring (with injection and extraction systems)

There are four RTML system MAD files (in comfiles/RTML):

 eRTML.mad8       : e- RTL, LTL, TURN, and SPIN lines
                    (including RTL, BC1, and BC2 extraction/tuneup/dump lines)
 eRTML_match.mad8 : MAD matching subroutines

 pRTML.mad8       : e+ RTL, LTL, TURN, and SPIN lines
                    (including RTL, BC1, and BC2 extraction/tuneup/dump lines)
 pRTML_match.mad8 : MAD matching subroutines

There are two Main Linac system MAD file (in comfiles/MainLinac):

 eLIN.mad8 : 15-250 GeV e- linac, MPS collimation, undulator, and dogleg lines

 pLIN.mad8 : 15-250 GeV e+ linac and MPS collimation lines

There are two Beam Delivery system MAD files and two Extended Standard Input
Format files (in comfiles/BeamDelivery):

 eBDS.mad8  : e- BSY, Final Focus, and primary dump lines
             (including BSY extraction/tuneup/dump line)
 eBDS.xsif : XSIF compliant input extracted from eBDS.mad8

 pBDS.mad8 : e+ BSY, Final Focus, and primary dump lines
             (including BSY extraction/tuneup/dump line)
 pBDS.xsif : XSIF compliant input extracted from pBDS.mad8

There are six LET system MAD files (in comfiles/LET):

 eLET.mad8       : e- RTML, Main Linac, and BDS lines
                   (including pre-undulator extraction/tuneup/dump line, and
                   a "photon line" from undulator end to e+ production target)
 eLET_match.mad8 : MAD matching subroutines
 eDumpLines.mad8 : for generating SURVEY and Twiss files for abort/tuneup lines

 pLET.mad8       : e+ RTML, Main Linac, and BDS lines
 pLET_match.mad8 : MAD matching subroutines
 pDumpLines.mad8 : for generating SURVEY and Twiss files for abort/tuneup lines

Documentation (such as it is) may be found in the doc/ folder.

================================================================================

Mark Woodley
SLAC
mdw@SLAC.Stanford.EDU